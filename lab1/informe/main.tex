\input{preamble}

\pagenumbering{roman}

\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\title{
{
    \begin{tikzpicture}[overlay, remember picture]
        \node[anchor=north west, %anchor is upper left corner of the graphic
            xshift=3cm, %shifting around
            yshift=-3.45cm] 
            at (current page.north west) %left upper corner of the page
        {\includegraphics[height=1.9cm]{img/EIE_v.png}}; 
    \end{tikzpicture}
    \begin{tikzpicture}[overlay, remember picture]
        \node[anchor=north east, %anchor is upper left corner of the graphic
            xshift=-2.5cm, %shifting around
            yshift=-3.65cm] 
            at (current page.north east) %left upper corner of the page
        {\includegraphics[height=1.4cm]{img/ucr.png}}; 
    \end{tikzpicture}
    \Large 
        \textbf{Universidad de Costa Rica}\\
        Facultad de Ingeniería\\
        Escuela de Ingeniería Eléctrica\\~\\
        \vspace{3cm} \texttt{IE-0624} Laboratorio de Microcontroladores
    }
    ~\\~\\\vspace{3cm} 
    {\textbf{\LARGE Laboratorio 1}}\\
    \vspace{3cm}
}

\pagestyle{fancy}
\fancyhf{}
\lhead{IE-0624: Laboratorio de Microcontroladores}
\chead{}
\rhead{Laboratorio 1}
\lfoot{Escuela de Ingeniería Eléctrica}
% \cfoot{\thepage\ de \pageref{LastPage}}
\cfoot{\thepage}
\rfoot{Universidad de Costa Rica}

\author{Victor Manuel Yeom Song - \texttt{B78494}
    }

\date{II-2020}

\makeatletter
\g@addto@macro{\UrlBreaks}{\UrlOrds}
\makeatother
\begin{document}

\maketitle

\newpage
\tableofcontents

\newpage
\pagenumbering{arabic}

\section{Resumen}

En este laboratorio se realiza una introducción al uso de las \textit{General Purpose Input Output} (GPIOs). Esto se realizó mediante el uso del microcontrolador PIC12f675, utilizado para simular el lanzamiento de un dado. La simulación consiste en la generación de un número entre 1 y 6 al presionar un botón, lo cual se visualiza con la activación de 6 LEDs.

Mediante el desarrollo del laboratorio se notó que el manejo del microcontrolador es sencillo, requiriendo pocas líneas de código en C para configurar correctamente el funcionamiento del mismo. Por esto, se puede facilitar el desarrollo de proyectos como el que se presenta en este laboratorio.

Durante la simulación del circuito en SimulIDE pueden surgir algunas complicaciones debido a diversos \textit{bugs} de la herramienta. Aún así, se lograron obtener simulaciones que logran confirmar el correcto desarrollo del diseño solicitado. Los parámetros eléctricos medidos no correspondieron completamente a los diseñados, pero eso se puede deber al uso de modelos simplificados que no toman en cuenta la complejidad real de los diodos.

\newpage
\section{Nota teórica}

\subsection{El microcontrolador PIC12f675}
El microcontrolador utilizado para el desarrollo de este laboratorio es el PIC12f675. En \cite{PIC} se indica que es un microcontrolador CMOS con 6 pines de entrada/salida (E/S) y 2 pines para alimentación. Su diagrama se presenta en la figura \ref{fig:picPins}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .8\linewidth]{img/background/picPins.png}
    \caption{Diagrama del PIC12f675. Recuperado de \cite{PIC}.}
    \label{fig:picPins}
\end{figure}

En \cite{PIC} se indica que el microcontrolador cuenta con un procesador de arquitectura RISC de 35 instrucciones, reloj de \SI{20}{MHz}, rango de operación de tensión entre \SI{2}{V} y \SI{5,5}{V}, un oscilador interno, capacidad para un oscilador externo y modo de ahorro de potencia. Tiene una corriente de reposo de \SI{1}{nA} operando a \SI{2}{V} y una corriente de operación de \SI{8,5}{\micro A} si opera a \SI{32}{kHz}, o bien de \SI{100}{\micro A} si opera a \SI{1}{MHz}.

En cuanto a capacidades periféricas, los 6 pines del microcontrolador cuentan con capacidades de drenaje/fuente de corriente capaces de controlar LEDs, comparación de tensiones, módulo convertidor de analógico a digital, entre otros. La funcionalidad de los pines se configura por medio de los registros del microcontrolador, los cuales se muestran en la figura \ref{fig:REGs}.

Para este laboratorio, son de principal interés los pines TRISIO, que define si los pines se utilizan como entrada (1) o salida (0), así como los GPIO, que definen si las salidas están en alto (1) o bajo (0). Uno los puede programar en C y definir cuáles son los pines con 1 o 0 en las variables proveídas \mintinline{C}{TRISIO} y \mintinline{C}{GPIO}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .28\linewidth]{img/background/REGs.png}
    \caption{Registros del PIC12f675. Recuperado de \cite{PIC}.}
    \label{fig:REGs}
\end{figure}


\subsection{El diodo semiconductor}

Los diodos semiconductores son dispositivos que permiten el flujo de la corriente en una dirección pero no en el otro. En \cite{Boylestad} se indica que son creados a partir de la unión de un material tipo p (silicio dopado con algún elemento como el boro) con otro tipo n (silicio dopado con algún elemento como el fósforo). También se indica que si uno deseara un LED, simplemente se agregan materiales al silicio o el encapsulado que reaccionan a los campos eléctricos y generan luz.

La ecuación que describe el comportamiento del diodo es dada por \eqref{eqn:diode}:

\begin{equation} \label{eqn:diode}
    I_D=I_s \left (e^{\frac{V_D}{V_T}}-1 \right)
\end{equation}

Donde $I_D$ es la corriente a través del diodo, $I_S$ es la corriente de saturación inversa, $V_D$ la tensión a través del diodo y $V_T$ el voltaje térmico, determinado por \eqref{eqn:threshold}:

\begin{equation}\label{eqn:threshold}
    V_T=\frac{\kappa T}{q}
\end{equation}

Donde $\kappa$ es la constante de Boltzmann, T la temperatura en kelvins y q la carga del electrón.

\subsection{Rebote de switches}

En \cite{Bounce} se indica que en la vida real, cuando se presiona un botón o switch, se produce el contacto de dos partes de metal. Este contacto puede parecer instantáneo, pero durante el momento de activación del botón se pueden dar varios contactos pequeños antes del cierre total del botón o switch, por lo que las señales eléctricas que produce el switch ``rebotan'' entre apagado y encendido en los momentos previos al cierre total del mismo. Se pueden producir entre 10 y 100 de estos rebotes en \SI{1}{ms}, lo cual resulta problemático dado que el hardware utilizado en la actualidad puede operar a velocidades más altas, por lo que puede interpretar esos rebotes como varias activaciones del switch.

Para lidiar con estos rebotes, generalmente se utilizan capacitores para que filtren estas oscilaciones. Para el laboratorio, además de un capacitor, se utiliza una resistencia para limitar la corriente que se puede producir a la entrada del microcontrolador, así como facilitar la carga y descarga segura del capacitor cuando se presiona el botón. La adición de estos elementos no produciría un efecto muy visible en la simulación en la herramienta SimulIDE, dado que los switches en la misma son ideales y no producen rebote, pero se toman en cuenta para una eventual implementación en la vida real.

\section{Lista de componentes}

La lista de componentes necesarios para desarrollar el diseño se presentan en el cuadro \ref{tab:components}.

\begin{table}[H]
    \centering
    \begin{tabular}{cccc}
        \toprule
        Componente & Valor nominal & Precio & Cantidad \\ \midrule
        Resistencia \SI{1/4}{W} & \SI{200}{\ohm} & \$5,4 / 10 piezas & 9 \\ \hline
        \multirow{2}{*}{LED TLUR6400} & $V_{fw} = \SI{2}{V}$ & \multirow{2}{*}{\$3,30 / 10 piezas} & \multirow{2}{*}{6} \\
        &  $I_{fw, max} = \SI{20}{mA}$ & & \\\hline
        Capacitor cerámico & \SI{47}{nF} & \$0,25 / 1 pieza & 1 \\\hline
        PIC12f675 & --- & \$1,13 / 1 unidad & 1 \\ \bottomrule
    \end{tabular}
    \caption{Lista de componentes para realizar el diseño.}
    \label{tab:components}
\end{table}

La información sobre los costos y aspectos de los componentes se obtuvieron en las siguientes direcciones URL:

\begin{itemize}
    \item Resistencia: \newline \url{https://www.amazon.com/Projects-Resistors-Watt-Choose-Quantity/dp/B071HLFRF5}
    
    \item LED: \url{https://www.mouser.co.cr/ProductDetail/Vishay-Semiconductors/TLUR6400?qs=\%2Fha2pyFaduheQTaVa\%252BYMbL2buDTtPc2eNvmnn2P4l2M=}
    
    \item Capacitor cerámico: \url{https://www.jameco.com/z/DC-047-Capacitor-Ceramic-Disc-0-047-micro-F-47nF-50V-plusmn-20-_15253.html}
    
    \item PIC: \cite{PIC} y \url{https://www.mouser.co.cr/ProductDetail/Microchip-Technology/PIC12F675-I-P?qs=g6BBfX6YTklkFbwYwZUX2A==}
\end{itemize}

\section{Diseño}

Para realizar una generación aleatoria de un número entre 1 y 6, se implementa un código relativamente simple. Se utiliza un contador entero, que inicia en 1 y en cada iteración del \textit{loop} infinito con el que corre el microcontrolador se le suma 1. Cuando el contador llega a 7, se reinicia su valor a 1. El microcontrolador revisa constantemente si se presiona un botón con un condicional \textit{if}, en cada iteración del \textit{loop}. En términos humanos, cada iteración dura muy poco, al punto en el que sería muy difícil, si no imposible, determinar cuál número está cargado en un determinado momento, por lo que para efectos prácticos el número que se obtiene es aleatorio.

Con lo anterior, se plantea un diseño con 4 ``ramas'' de LEDs: dos de 2 LEDs y 2 de 1 LED. Así, se pueden realizar combinaciones de ramas para sumar los números del 1 al 6. Cada una de las ramas es controlada por uno de 6 de los GPIO del microcontrolador, por lo que se tendrían 4 pines en modo de salida. También se tendría 1 pin en modo de entrada, para detectar la activación del botón. Con estas 4 ramas, se tienen las siguientes combinaciones para cada número:

\begin{itemize}
    \item 1: una rama de 1 LED
    \item 2: una rama de 2 LEDs
    \item 3: una rama de 1 LED y una rama de 2 LEDs
    \item 4: dos ramas de 2 LEDs cada una
    \item 5: dos ramas de 2 LEDs cada una y una rama de 1 LED
    \item 6: todas las ramas
\end{itemize}

Cada rama debe tener una resistencia asociada que limite la corriente que corre por los LEDs (con corriente de \textit{forward} máxima de \SI{20}{mA} como se indica en \cite{LED}), así como limitar la corriente que consumen dado que el PIC12f675 tiene una corriente de suministro máxima de \SI{125}{mA} entre todos los pines GPIO \cite{PIC}). Como se utilizan cuatro pines de salida, siempre y cuando se mantenga una corriente menor a \SI{20}{mA} en cada rama, no deberían haber problemas en este aspecto. También se tiene que por default cada pin de salida produce una tensión de \SI{5}{V}.

Se analiza cada tipo de rama por separado; primero se tiene la rama de una resistencia y un LED, dada en la figura \ref{fig:designR_1LED}:

\begin{figure}[H]
    \centering
    \includegraphics[width = .4\linewidth]{img/design/designR_1LED.png}
    \caption{Circuito de diseño para la rama de 1 LED. Elaboración propia.}
    \label{fig:designR_1LED}
\end{figure}

Nótese que aquí se toma el modelo simplificado del diodo y no se toman en cuenta sus propiedades resistivas y capacitivas, lo cual puede inducir un error en los resultados finales. Estos aspectos físicos no se toman en cuenta dado que pueden variar mucho entre diodo y diodo, no se indican en \cite{LED} y la relación entre corriente y tensión no es lineal, como se modela por la ecuación del diodo de Shockley.

Se define una corriente $I_{1D} = \SI{15}{mA}$, menor a la corriente máxima de \SI{20}{mA} del LED, para diseñar la resistencia R. Por una LTK, se tiene:

\begin{eqnarray*}
    I_{1D}R + 2 &=& 5\\
    \iff R &=& \frac{5-2}{I_{1D}}\\
    \iff R &=& \SI{200}{\ohm}
\end{eqnarray*}

Entonces en cada rama de solo 1 LED se utiliza una resistencia de \SI{200}{\ohm}. Luego, para las ramas de 2 LEDs se tiene el circuito de diseño dado en la figura \ref{fig:designR_2LED}:

\begin{figure}[H]
    \centering
    \includegraphics[width = .4\linewidth]{img/design/designR_2LED.png}
    \caption{Circuito de diseño para la rama de 2 LEDs. Elaboración propia.}
    \label{fig:designR_2LED}
\end{figure}

Similar al diseño de las ramas de 1 LED, se asume una corriente $I_{2D} = \SI{15}{mA}$. Por una LTK, se tiene:

\begin{eqnarray*}
    I_{2D}R + 2 + 2 &=& 5\\
    \iff R &=& \frac{5-4}{I_{2D}}\\
    \iff R &=& \frac{200}{3} \text{ $\Omega$} \approx \SI{66,67}{\ohm}
\end{eqnarray*}

Este valor de resistencia se puede obtener mediante el paralelo de 3 resistencias de \SI{200}{\ohm} cada una, por lo que se puede realizar el diseño únicamente con resistencias de \SI{200}{\ohm}. 

Ahora, para evitar el efecto del rebote del botón que se tendría en la vida real, como se hace en \cite{Bounce}, simplemente se agrega un circuito RC asociado al botón. El capacitor se encarga de filtrar los pulsos rápidos del rebote, mientras que la resistencia sirve como la ruta de carga y descarga del capacitor, además de limitar la corriente que entra al microcontrolador. Se toman una capacitancia de \SI{47}{nF} y una resistencia de \SI{200}{\ohm} para tener una constante de tiempo pequeña, de manera que el ``delay'' entre la entrada y la salida sea pequeña. La resistencia limitaría la corriente producida en la entrada a \SI{25}{mA}, por lo que asegura su seguridad (en \cite{PIC} se indica que la corriente máxima de entrada del PIC12f675 es de \SI{125}{mA} en sus entradas GPIO).

Para el diseño final, se tiene el circuito de la figura \ref{fig:rngDesign}, implementado en SimulIDE:

\begin{figure}[H]
    \centering
    \includegraphics[width = .7\linewidth]{img/design/rngDesign.png}
    \caption{Circuito diseñado final. Elaboración propia.}
    \label{fig:rngDesign}
\end{figure}

Entonces, se ve que se definió el pin de GPIO 5 como la entrada al botón, mientras que las salidas son los pines 0, 1, 2 y 4.

\section{Análisis de resultados}

A continuación, se presentan los resultados de presionar el botón algunas veces con tensiones y corrientes en algunas ramas. Primero, se tiene la tensión en una rama de 1 LED en la figura \ref{fig:voltage_1LED}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .7\linewidth]{img/results/voltageBranch_1LED.png}
    \caption{Tensión en una rama de 1 LED, con un 3 generado. Elaboración propia.}
    \label{fig:voltage_1LED}
\end{figure}

\newpage
También se tiene la tensión en una rama de 2 LEDs en la figura \ref{fig:voltage_2LED}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .7\linewidth]{img/results/voltageBranch_2LED.png}
    \caption{Tensión en una rama de 1 LED, con un 4 generado. Elaboración propia.}
    \label{fig:voltage_2LED}
\end{figure}

Se puede notar que en los casos mostrados, la tensión de las ramas no es de \SI{5}{V}. Esto se puede deber a las propiedades resistivas y capacitivas de los diodos que no se tomaron en cuenta al realizar el diseño. Estos parámetros físicos pueden afectar la corriente que se entrega a cada rama y por tanto la tensión en la resistencia. Para verificar esto, se mide la corriente en una rama, como se muestra en la figura \ref{fig:curr_1LED}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .7\linewidth]{img/results/currBranch.png}
    \caption{Corriente en una rama de 1 LED, con un 6 generado. Elaboración propia.}
    \label{fig:curr_1LED}
\end{figure}

Aquí se confirma que no se obtienen los \SI{15}{mA} para los que se habían diseñado originalmente las resistencias. Sin embargo, sigue existiendo suficiente corriente para encender los LEDs y, como no es mayor a la del diseño, no existen riesgos de sobrepasar las medidas máximas de los componentes utilizados. 

Aunado a lo anterior, se ve que el circuito diseñado funciona de manera esperada, con los números generados aleatoriamente mostrándose de forma clara en los LEDs. Aunque se sabe que ``por debajo'' los números siguen una secuencia, el usuario promedio no tendría forma de saber cuál número de la secuencia se da en un momento determinado, y para efectos prácticos se vería como un número aleatorio. Por esto, se considera que no hay necesidad de realizar un rediseño del circuito y si fuera el caso de que se desea más corriente en cada rama, simplemente se utilizan resistencias de menor valor nominal.

\section{Conclusiones y recomendaciones}

Con este laboratorio se observó que el uso de GPIOs con el microcontrolador PIC12f675 es tanto fácil como versátil en el desarrollo de aplicaciones. Esta versatilidad es dada gracias a su capacidad de programación en C y la disponibilidad de compiladores como sdcc facilitan la traslación de código al hardware.

Para el diseño de circuitos siempre es bueno tomar en cuenta las características y capacidades de los componentes utilizados. Siempre se deben medir las capacidades eléctricas de cada componente utilizado para poder dimensionar bien el diseño, como se pudo ver con el uso de los LEDs en el circuito final.

% \bibliography{annot}

\begin{thebibliography}{IEEEannot}

\bibitem{Bounce} Jens Christoffersen. ``Switch Bounce and How to Deal with It'', 2015. [En línea]. Disponible en: \url{https://www.allaboutcircuits.com/technical-articles/switch-bounce-how-to-deal-with-it/}

\bibitem{PIC} Microchip Devices, ``PIC12F629/675 Data Sheet'', 2003.

\bibitem{Boylestad} Robert L. Boylestad \& Louis Nashelsky, \textit{Electrónica: Teoría de Circuitos y Dispositivos Electrónicos}. Pearson Education, 2009.

\bibitem{LED} Vishay Semiconductors, ``Universal LED in $\varnothing$ \SI{5}{mm} Tinted Difussed Package'', 2013. [En línea]. Disponible en: \url{https://www.vishay.com/docs/83171/tlur640.pdf}

\end{thebibliography}

\end{document}
