#include <pic14/pic12f675.h>
 
void delay (unsigned int tiempo);
 
void main(void)
{

    TRISIO = 0b00100000; //Poner el pin 5 como entrada y el resto como salidas
	GPIO = 0x00; //Poner pines en bajo
 
    unsigned int time = 1000;
	unsigned int ctr = 1; // Contador que irá de 1 a 6 para representar el dado
 
    //Loop forever
    while ( 1 )
    {

		if(ctr >= 7){ //Si el contador llega a 7 se reinicia a 1
			ctr = 1;
		}

		// Si se presiona el botón, GP5 se vuelve igual a 1
		if(GP5 == 1){
			// El switch-case cubre bastante bien cada caso, ya que se conocen bien
			switch(ctr){
				case 1:
					GPIO = 0b00000001; // Se enciende una rama con solo un LED
					break;

				case 2:
					GPIO = 0b00000010; // Se enciende una rama con 2 LEDs
					break;

				case 3:
					GPIO = 0b00000011; // Se enciende una rama con 2 LEDs y una rama con 1 LED
					break;
				
				case 4:
					GPIO = 0b00000110; // Se encienden las 2 ramas con 2 LEDs cada una
					break;
				
				case 5:
					GPIO = 0b00000111; // Se encienten las 2 ramas de 2 LEDs y una rama de 1 LED
					break;
				
				case 6:
					GPIO = 0b00010111; // Se encienden todas las ramas
					break;
			}

			delay(time); // Se espera un tiempo antes de apagar el LED
			GPIO = 0x00; // Las salidas se apagan después de que pasa el periodo de tiempo definido
		}

		ctr++; // Se aumenta el contador para rotar a través de los 6 valores posibles
    }
 
}

// Función para esperar un tiempo, dado del ejemplo holaPIC disponible en el sitio virtual del curso
void delay(unsigned int tiempo)
{
	unsigned int i;
	unsigned int j;

	for(i=0;i<tiempo;i++)
	  for(j=0;j<1275;j++);
}
