;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.0 #11528 (Linux)
;--------------------------------------------------------
; PIC port for the 14-bit core
;--------------------------------------------------------
;	.file	"rng.c"
	list	p=12f675
	radix dec
	include "p12f675.inc"
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	extern	_TRISIO
	extern	_GPIO
	extern	_GPIObits
	extern	__sdcc_gsinit_startup
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	global	_main
	global	_delay

	global PSAVE
	global SSAVE
	global WSAVE
	global STK12
	global STK11
	global STK10
	global STK09
	global STK08
	global STK07
	global STK06
	global STK05
	global STK04
	global STK03
	global STK02
	global STK01
	global STK00

sharebank udata_ovr 0x0020
PSAVE	res 1
SSAVE	res 1
WSAVE	res 1
STK12	res 1
STK11	res 1
STK10	res 1
STK09	res 1
STK08	res 1
STK07	res 1
STK06	res 1
STK05	res 1
STK04	res 1
STK03	res 1
STK02	res 1
STK01	res 1
STK00	res 1

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
UDL_rng_0	udata
r0x1008	res	1
r0x1009	res	1
r0x100A	res	1
r0x1001	res	1
r0x1000	res	1
r0x1002	res	1
r0x1003	res	1
r0x1004	res	1
r0x1005	res	1
r0x1006	res	1
r0x1007	res	1
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
;--------------------------------------------------------
; initialized absolute data
;--------------------------------------------------------
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
;--------------------------------------------------------
; reset vector 
;--------------------------------------------------------
STARTUP	code 0x0000
	nop
	pagesel __sdcc_gsinit_startup
	goto	__sdcc_gsinit_startup
;--------------------------------------------------------
; code
;--------------------------------------------------------
code_rng	code
;***
;  pBlock Stats: dbName = M
;***
;has an exit
;functions called:
;   _delay
;   _delay
;4 compiler assigned registers:
;   r0x1008
;   r0x1009
;   r0x100A
;   STK00
;; Starting pCode block
S_rng__main	code
_main:
; 2 exit points
;	.line	8; "rng.c"	TRISIO = 0b00100000; //Poner el pin 5 como entrada y el resto como salidas
	MOVLW	0x20
	BANKSEL	_TRISIO
	MOVWF	_TRISIO
;	.line	9; "rng.c"	GPIO = 0x00; //Poner pines en bajo
	BANKSEL	_GPIO
	CLRF	_GPIO
;	.line	12; "rng.c"	unsigned int ctr = 1; // Contador que irá de 1 a 6 para representar el dado
	MOVLW	0x01
	MOVWF	r0x1008
	CLRF	r0x1009
;;unsigned compare: left < lit(0x7=7), size=2
_00117_DS_:
;	.line	18; "rng.c"	if(ctr >= 7){ //Si el contador llega a 7 se reinicia a 1
	MOVLW	0x00
	SUBWF	r0x1009,W
	BTFSS	STATUS,2
	GOTO	_00141_DS_
	MOVLW	0x07
	SUBWF	r0x1008,W
_00141_DS_:
	BTFSS	STATUS,0
	GOTO	_00106_DS_
;;genSkipc:3307: created from rifx:0x7fff2b2a97a0
;	.line	19; "rng.c"	ctr = 1;
	MOVLW	0x01
	MOVWF	r0x1008
	CLRF	r0x1009
_00106_DS_:
;	.line	23; "rng.c"	if(GP5 == 1){
	CLRF	r0x100A
	BANKSEL	_GPIObits
	BTFSC	_GPIObits,5
	INCF	r0x100A,F
	MOVF	r0x100A,W
	XORLW	0x01
	BTFSS	STATUS,2
	GOTO	_00115_DS_
;;unsigned compare: left < lit(0x1=1), size=2
;	.line	25; "rng.c"	switch(ctr){
	MOVLW	0x00
	SUBWF	r0x1009,W
	BTFSS	STATUS,2
	GOTO	_00142_DS_
	MOVLW	0x01
	SUBWF	r0x1008,W
_00142_DS_:
	BTFSS	STATUS,0
	GOTO	_00113_DS_
;;genSkipc:3307: created from rifx:0x7fff2b2a97a0
;;swapping arguments (AOP_TYPEs 1/2)
;;unsigned compare: left >= lit(0x7=7), size=2
	MOVLW	0x00
	SUBWF	r0x1009,W
	BTFSS	STATUS,2
	GOTO	_00143_DS_
	MOVLW	0x07
	SUBWF	r0x1008,W
_00143_DS_:
	BTFSC	STATUS,0
	GOTO	_00113_DS_
;;genSkipc:3307: created from rifx:0x7fff2b2a97a0
	DECF	r0x1008,W
	MOVWF	r0x100A
	MOVLW	HIGH(_00144_DS_)
	MOVWF	PCLATH
	MOVLW	_00144_DS_
	ADDWF	r0x100A,W
	BTFSC	STATUS,0
	INCF	PCLATH,F
	MOVWF	PCL
_00144_DS_:
	GOTO	_00107_DS_
	GOTO	_00108_DS_
	GOTO	_00109_DS_
	GOTO	_00110_DS_
	GOTO	_00111_DS_
	GOTO	_00112_DS_
_00107_DS_:
;	.line	27; "rng.c"	GPIO = 0b00000001; // Se enciende una rama con solo un LED
	MOVLW	0x01
	BANKSEL	_GPIO
	MOVWF	_GPIO
;	.line	28; "rng.c"	break;
	GOTO	_00113_DS_
_00108_DS_:
;	.line	31; "rng.c"	GPIO = 0b00000010; // Se enciende una rama con 2 LEDs
	MOVLW	0x02
	BANKSEL	_GPIO
	MOVWF	_GPIO
;	.line	32; "rng.c"	break;
	GOTO	_00113_DS_
_00109_DS_:
;	.line	35; "rng.c"	GPIO = 0b00000011; // Se enciende una rama con 2 LEDs y una rama con 1 LED
	MOVLW	0x03
	BANKSEL	_GPIO
	MOVWF	_GPIO
;	.line	36; "rng.c"	break;
	GOTO	_00113_DS_
_00110_DS_:
;	.line	39; "rng.c"	GPIO = 0b00000110; // Se encienden las 2 ramas con 2 LEDs cada una
	MOVLW	0x06
	BANKSEL	_GPIO
	MOVWF	_GPIO
;	.line	40; "rng.c"	break;
	GOTO	_00113_DS_
_00111_DS_:
;	.line	43; "rng.c"	GPIO = 0b00000111; // Se encienten las 2 ramas de 2 LEDs y una rama de 1 LED
	MOVLW	0x07
	BANKSEL	_GPIO
	MOVWF	_GPIO
;	.line	44; "rng.c"	break;
	GOTO	_00113_DS_
_00112_DS_:
;	.line	47; "rng.c"	GPIO = 0b00010111; // Se encienden todas las ramas
	MOVLW	0x17
	BANKSEL	_GPIO
	MOVWF	_GPIO
_00113_DS_:
;	.line	51; "rng.c"	delay(time); // Se espera un tiempo antes de apagar el LED
	MOVLW	0xe8
	MOVWF	STK00
	MOVLW	0x03
	PAGESEL	_delay
	CALL	_delay
	PAGESEL	$
;	.line	52; "rng.c"	GPIO = 0x00; // Las salidas se apagan después de que pasa el periodo de tiempo definido
	BANKSEL	_GPIO
	CLRF	_GPIO
_00115_DS_:
;	.line	55; "rng.c"	ctr++; // Se aumenta el contador para rotar a través de los 6 valores posibles
	INCF	r0x1008,F
	BTFSC	STATUS,2
	INCF	r0x1009,F
	GOTO	_00117_DS_
;	.line	58; "rng.c"	}
	RETURN	
; exit point of _main

;***
;  pBlock Stats: dbName = C
;***
;has an exit
;9 compiler assigned registers:
;   r0x1000
;   STK00
;   r0x1001
;   r0x1002
;   r0x1003
;   r0x1004
;   r0x1005
;   r0x1006
;   r0x1007
;; Starting pCode block
S_rng__delay	code
_delay:
; 2 exit points
;	.line	60; "rng.c"	void delay(unsigned int tiempo)
	MOVWF	r0x1000
	MOVF	STK00,W
	MOVWF	r0x1001
;	.line	65; "rng.c"	for(i=0;i<tiempo;i++)
	CLRF	r0x1002
	CLRF	r0x1003
_00155_DS_:
	MOVF	r0x1000,W
	SUBWF	r0x1003,W
	BTFSS	STATUS,2
	GOTO	_00176_DS_
	MOVF	r0x1001,W
	SUBWF	r0x1002,W
_00176_DS_:
	BTFSC	STATUS,0
	GOTO	_00157_DS_
;;genSkipc:3307: created from rifx:0x7fff2b2a97a0
;	.line	66; "rng.c"	for(j=0;j<1275;j++);
	MOVLW	0xfb
	MOVWF	r0x1004
	MOVLW	0x04
	MOVWF	r0x1005
_00153_DS_:
	MOVLW	0xff
	ADDWF	r0x1004,W
	MOVWF	r0x1006
	MOVLW	0xff
	MOVWF	r0x1007
	MOVF	r0x1005,W
	BTFSC	STATUS,0
	INCFSZ	r0x1005,W
	ADDWF	r0x1007,F
	MOVF	r0x1006,W
	MOVWF	r0x1004
	MOVF	r0x1007,W
	MOVWF	r0x1005
	MOVF	r0x1007,W
	IORWF	r0x1006,W
	BTFSS	STATUS,2
	GOTO	_00153_DS_
;	.line	65; "rng.c"	for(i=0;i<tiempo;i++)
	INCF	r0x1002,F
	BTFSC	STATUS,2
	INCF	r0x1003,F
	GOTO	_00155_DS_
_00157_DS_:
;	.line	67; "rng.c"	}
	RETURN	
; exit point of _delay


;	code size estimation:
;	  121+   12 =   133 instructions (  290 byte)

	end
