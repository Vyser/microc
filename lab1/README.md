Laboratorio 1
==========
--------------------
Estudiante:
--------------------
+ Victor Manuel Yeom Song - B78494

En este repositorio se encuentra el trabajo realizado para el Laboratorio 1 del curso IE-0624 Laboratorio de Microcontroladores. 

En el directorio src se hallan el código fuente y los archivos de simulación necesarios para la simulación del diseño en SimulIDE. El archivo rng.simu contiene el circuito implementado en SimulIDE y el archivo rng.c el código implementado en C para la funcionalidad del PIC12f675. 

Para poder realizar la simulación, se debe compilar el archivo rng.c (el Makefile de src se encarga de esto) de donde se obtiene un archivo de extensión .hex, llamado "rng.hex". Luego, con el archivo rng.simu abierto en SimulIDE, se debe cargar el *firmware* al microcontrolador. Este firmware corresponde al archivo rng.hex.

En el directorio informe se hallan los archivos necesarios para generar el informe del laboratorio. Se incluye un Makefile que compila los archivos y abre el documento pdf generado.