#include <Keypad.h>

const byte ROWS = 4; //four rows
const byte COLS = 3; //three columns
char keys[ROWS][COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'*','0','#'}
};

bool door_just_opened = false;
bool pw_status = false;
char password[4] = {'1','2','3','4'};
char input[4] = {'f','f','f','f'};
int itr = 0;
unsigned long pw_time_start = 0;
char key = 'f';
byte rowPins[ROWS] = {5, 4, 3, 9}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {8, 7, 6}; //connect to the column pinouts of the keypad

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

bool correct_pw();

void door();

void setup(){
  Serial.begin(9600);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(2, INPUT);
  attachInterrupt(digitalPinToInterrupt(2), door, RISING);
}
  
void loop(){

  if (!correct_pw()){
    digitalWrite(11, LOW);
    digitalWrite(12, HIGH);
    if(door_just_opened){
      Serial.println("INTRUDER!");
      door_just_opened = false;
    }
  }
  else {
    digitalWrite(12, LOW);
    digitalWrite(11, HIGH);
    if(door_just_opened){
      Serial.println("Authorized Entry");
      door_just_opened = false;
    }
  }
    
}

bool correct_pw(){
  key = keypad.getKey();
  if(key != NO_KEY){
    if( (key == '*') && (itr != 0) ){
      itr--;
      input[itr] = 'f';
    } else if (key == '#'){
      itr = 0;
      for (int i = 0; i < 4; i++){
        if (password[i] != input[i]){
          for (int i = 0; i < 4; i++){
            input[i] = 'f';
          }
          break;
        }
        if (i == 3){
          pw_status = true;
          pw_time_start = millis();
        }
      }
      for (int i = 0; i < 4; i++){
        input[i] = 'f';
      }
    } else if(itr < 4){
      input[itr] = key;
      itr++;
    }
  }

  // Ahora mantenemos el valor de checkPass en durante 5 s después de introducida la pw
  if (pw_status && (millis() - pw_time_start >= 6000)){
      pw_status = false;
  }
  
  return pw_status;
}

void door (){
  door_just_opened = true;
}
