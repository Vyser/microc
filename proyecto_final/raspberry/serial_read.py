#!/usr/bin/env python3
import serial
import subprocess
from picamera import PiCamera
from time import sleep
from datetime import datetime

if __name__ == '__main__':
    ser = serial.Serial('/dev/ttyS0', 9600, timeout=1)
    ser.flush()
    while True:
        if ser.in_waiting > 0:
            line = ser.readline().decode('utf-8').rstrip()
            print(line)
            if line == "INTRUDER!":
            
                # Momento cuando se toma la imagen y formato del nombre del archivo
                now = datetime.now()
                date = now.strftime("%Y-%m-%d-%H-%M-%S")
                imgPath = "./img/intruder-" + date + ".jpg"

                # Inicializacion de la camara y toma de la imagen
                camera = PiCamera()
                camera.start_preview()
                sleep(2)
                camera.capture(imgPath)
                camera.stop_preview()
                camera.close()

                # Interfaz con Bash para enviar un mensaje por medio de Telegram con la imagen
                bashCommand = "telegram-send --image " + imgPath + " --caption " + date
                process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
                output, error = process.communicate()