from serial import Serial
import paho.mqtt.client as mqtt
import json

# CALLBACKS:

def on_log (client , userdata , level , buf):
  print("log: "+buf )

def on_connect ( client, userdata , flags , rc ):
  if rc ==0:
    print("connected OK")
  else :
    print("Bad connection Returned code=" , rc )

def on_disconnect ( client, userdata , flags , rc = 0 ):
  print("DisConnected result code "+ str( rc ) )

def on_message ( client , userdata ,msg ):
  topic=msg. topic
  m_decode= str(msg. payload .decode("utf-8" ,"ignore" ) )
  print("message received" ,m_decode)


ser = Serial('/tmp/ttyS1', '9600',timeout=1) #Se abre la comunicación serial. Se debe cambiar el puerto.

broker="iot.eie.ucr.ac.cr"
port=1883
topic="v1/devices/me/telemetry"
username="vTwzzil1HCTIGXTtJxaZ"
password=""

client = mqtt.Client("Estacion_meteorologica")
client.on_connect = on_connect
client.on_disconnect = on_disconnect
client.username_pw_set(username, password)
client.connect(broker,port)

while True:

	line = ser.readline() #Se lee del puerto serial, y se verifica si no es vacío
	if line:
		print("Data: \n")
		decoded_bytes = float(line[0:len(line)-2].decode("utf-8"))
		print(decoded_bytes)
		if decoded_bytes==0.0:
			decoded_bytes = "Si"
		else:
			decoded_bytes = "No"
		dictionary = {"lluvia":decoded_bytes}
		decoded_bytes = json.dumps(dictionary)
		client.publish(topic,decoded_bytes)
		
		line = ser.readline()
		if line:
			decoded_bytes = float(line[0:len(line)-2].decode("utf-8"))
			print(decoded_bytes)
			dictionary = {"Velocidad del viento":decoded_bytes}
			decoded_bytes = json.dumps(dictionary)
			client.publish(topic,decoded_bytes)
			
		line = ser.readline()
		if line:
			decoded_bytes = float(line[0:len(line)-2].decode("utf-8"))
			print(decoded_bytes)
			dictionary = {"Luminosidad":decoded_bytes}
			decoded_bytes = json.dumps(dictionary)
			client.publish(topic,decoded_bytes)
			
		line = ser.readline()
		if line:
			decoded_bytes = float(line[0:len(line)-2].decode("utf-8"))
			print(decoded_bytes)
			dictionary = {"Humedad":decoded_bytes}
			decoded_bytes = json.dumps(dictionary)
			client.publish(topic,decoded_bytes)


		line = ser.readline()
		if line:
			decoded_bytes = float(line[0:len(line)-2].decode("utf-8"))
			print(decoded_bytes)
			dictionary = {"Temperatura":decoded_bytes}
			decoded_bytes = json.dumps(dictionary)
			client.publish(topic,decoded_bytes)


		line = ser.readline()
		if line:
			decoded_bytes = float(line[0:len(line)-2].decode("utf-8"))
			print(decoded_bytes)
			if decoded_bytes==0.0:
				decoded_bytes = "No"
			else:
				decoded_bytes = "Si"
			dictionary = {"Bateria_baja":decoded_bytes}
			decoded_bytes = json.dumps(dictionary)
			client.publish(topic,decoded_bytes)
		client.loop()
			