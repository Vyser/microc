#include <PCD8544.h>
#include <Servo.h> // Para el control de los servomotores
#include <EEPROM.h>
#include <LowPower.h>

Servo servo_x;
Servo servo_y;
int servo_x_val;
int servo_y_val;

// Variables booleanas para estado de la bateria
bool low_bat = false;
bool very_low_bat = false;  

static int sensors[] = {A0, A1, A2, A3, A4};
float measures[5];

// Variables para el tiempo
unsigned long tiempo_ref = 0;
unsigned long tiempo_actual = 0;
int intervalo_serial = 0; // para contar los 10 min
int screen_refresh_counter = 0;
int tiempo = 0;

// Timer
unsigned long timer;

//EEPROM address counter
int address = 0;

int batLevel = 0;

#define BYTES_INFO (3*sizeof(float) + sizeof(unsigned int) + sizeof(bool)) // Bytes requeridos para cada escritura al EEPROM

static PCD8544 lcd; // Manejo de la pantalla

// Funcion para impresion en pantalla LCD
void print_screen(void);

// Funcion general para obtener el parametro medido por los sensores
float calculate_param(float minAnalog, float maxAnalog, int readPin);

//Funcion para el envio de datos seriales
void sendSerial(void);

// Funcion para revisar el nivel de bateria y realizar un PowerDown de ser necesario
void checkBattery(int batLevel);

// Handler para el interrupt cuando la batería suministre mas de 7 V
void wakeUp();

// Funciones para el manejo de la EEPROM

void write_EEPROM(float temp, float hum, unsigned int lum, float vto, bool lluv);

void clean_EEPROM();

void setup() {

  //Servos           
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  Serial.begin(9600);

  // Resolucion de la pantalla
  lcd.begin(84, 48);

  // Sensores
  pinMode(sensors[0], INPUT); // Lluvia
  pinMode(sensors[1], INPUT); // Velocidad del viento
  pinMode(sensors[2], INPUT); // Luminosidad
  pinMode(sensors[3], INPUT); // Humedad
  pinMode(sensors[4], INPUT); // Temperatura

  pinMode(9, INPUT); // Envio de datos seriales
  pinMode(8, INPUT); // Uso de la pantalla

  // Manejo de servos por potenciometros
  pinMode(A6, INPUT);
  pinMode(A5, INPUT);

  servo_x.attach(10,1000, 2000);
  servo_y.attach(11,1000, 2000);

  pinMode(12, OUTPUT);
  pinMode(46, OUTPUT);

  //Regresa el tiempo transcurrido
    timer = millis();

  pinMode(A7, INPUT); // Lectura de la bateria

  // Si el wakeUpPin pasa de 0 V -> 5 V, se restableció la tensión 
  // y el Arduino sale de su estado de ahorro de energía
  attachInterrupt(digitalPinToInterrupt(18), wakeUp, RISING);
}

void loop() {

  // Servos
  servo_x_val = analogRead(PF6);
  servo_y_val = analogRead(PF5);
  servo_x_val = map(servo_x_val, 512, 1023, 0, 180);
  servo_y_val = map(servo_y_val, 512, 1023, 0, 180);
  servo_x.write(servo_x_val);
  servo_y.write(servo_y_val);
  measures[0] = calculate_param(0, 1, sensors[0]);
  measures[1] = calculate_param(0, 30, sensors[1]);
  measures[2] = calculate_param(1, 65535, sensors[2]);
  measures[3] = calculate_param(0, 100, sensors[3]);
  measures[4] = calculate_param(-40, 80, sensors[4]);
  batLevel = analogRead(PF7);
  tiempo_actual = millis();
  intervalo_serial = tiempo_actual - tiempo_ref;
    if(digitalRead(9)){
      digitalWrite(46, (millis() / 1000) % 2); //Parpadea cada segundo segun si el segundo es par o impar
      if(intervalo_serial>=6000){ // Cada 10 min
        tiempo_ref = tiempo_actual;
        sendSerial();
      }
    }
    if(digitalRead(8) && millis() - screen_refresh_counter >= 3000){ // Se refresca la pantalla cada 3 s
      lcd.clear();
      print_screen();
      screen_refresh_counter = millis();
    } else if (!digitalRead(8)){
      lcd.clear();
    }
  
   write_EEPROM(measures[4], measures[3], measures[2], measures[1], (bool) measures[0]);
   checkBattery(batLevel);
}

void print_screen(){
  lcd.setCursor(0, 0);
  lcd.print("Mediciones");
  lcd.setCursor(0, 1);
  lcd.print("RAIN: ");
  if(measures[0] > 0){
    lcd.print("No");
  }
  else{
    lcd.print("Yes");
  } 
  lcd.setCursor(0, 2);
  lcd.print("VEL (m/s): ");
  lcd.print((int)measures[1]);
  lcd.setCursor(0, 3);
  lcd.print("L (lx): ");
  lcd.print((long)measures[2]);
  lcd.setCursor(0, 4);
  lcd.print("HUM (%): ");
  lcd.print((int)measures[3]);
  lcd.setCursor(0, 5);
  lcd.print("TEMP (C): ");
  lcd.print((int)measures[4]);
}

float calculate_param(float minAnalog, float maxAnalog, int readPin){
  float calculatedVal;
  float normed = maxAnalog - minAnalog;
  if (readPin == A2){// Para el sensor de lluvia cuyo rango de salida es único: 0/+3 V
    calculatedVal = (analogRead(readPin)/613.8)*normed;
  }
  else{// Para los demas sensores
    calculatedVal = (analogRead(readPin)/1023.0)*normed;
  }
  return calculatedVal + minAnalog;
}

void sendSerial(){
  if (!very_low_bat){
    Serial.println(measures[0]);
    Serial.println(measures[1]);
    Serial.println(measures[2]);
    Serial.println(measures[3]);
    Serial.println(measures[4]);
  }
}

void write_EEPROM(float temp, float hum, float lum, float vto, bool lluv) {
    // Solo escribe si queda espacio en la EEPROM
    if (address <= EEPROM.length() - BYTES_INFO){
        if(millis()-timer >= 3000) {  //Revisa si es tiempo de escribir en el EEPROM
            EEPROM.put(address, temp);
            address += sizeof(float);
            EEPROM.put(address, hum);
            address += sizeof(float);
            EEPROM.put(address, lum);
            address += sizeof(float);
            EEPROM.put(address, vto);
            address += sizeof(float);
            EEPROM.put(address, lluv);
            address += sizeof(bool);
            timer = millis();
        }
    }
    else { // No hay espacio
        clean_EEPROM();
    }
 
}

void clean_EEPROM(){
  for (int i = 0 ; i < EEPROM.length() ; i++) {
    EEPROM.write(i, 0); // Limpia escribiendo 0
  }

    address = 0; //Reinicia el contador de direccion
}

void checkBattery(int batLevel) {
  float batVolt = batLevel * (12/1023.0);

  if(batVolt < 9.0) {

    low_bat = true;

    // Un rango de voltaje límite para enviar el mensaje de batería baja
    // pero el Arduino sigue funcionando
    if (batVolt >= 7.0) {
      digitalWrite(12, (millis() / 1000) % 2); //Parpadea cada segundo segun si el segundo es par o impar

      // La bateria esta baja pero es mayor a 7 V
      very_low_bat = false;

      // MANTENER ESTE EN 1 s, SOLO VALORES DE SENSORES DEBEN SER ENVIADOS CADA 10 MIN
      if(intervalo_serial >= 6000){
        if(digitalRead(9)){
          Serial.println(1);
        }
      }
    }
    // Si el voltaje es menor a 7 entra en estado de ahorro de energia
    else {
      very_low_bat = true;
      digitalWrite(12, LOW);
      
      // This is the lowest current consumption state.
      LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
    }
  }
  else {
    digitalWrite(12, LOW);
    low_bat = false;
    very_low_bat = false;

    // MANTENER ESTE EN 1 s, SOLO VALORES DE SENSORES DEBEN SER ENVIADOS CADA 10 MIN
    if(intervalo_serial>=6000){
      if(digitalRead(9)){
        Serial.println(0);
      }
    }
  }
}

void wakeUp(){
  // Un handler nada más, LowPower realiza el wake up respectivo
  // El interrupt flag se resetea automaticamente
}