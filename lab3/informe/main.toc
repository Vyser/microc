\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Resumen}{1}{section.1}%
\contentsline {section}{\numberline {2}Nota teórica}{2}{section.2}%
\contentsline {subsection}{\numberline {2.a}El microcontrolador ATmega328/P}{2}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.b}Arduino UNO}{3}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.c}Pantalla PCD8544}{4}{subsection.2.3}%
\contentsline {subsection}{\numberline {2.d}Convertidor Analógico-Digital (ADC)}{5}{subsection.2.4}%
\contentsline {subsection}{\numberline {2.e}Comunicación serial USART}{5}{subsection.2.5}%
\contentsline {subsection}{\numberline {2.f}El diodo semiconductor}{6}{subsection.2.6}%
\contentsline {subsection}{\numberline {2.g}Rebote de switches}{6}{subsection.2.7}%
\contentsline {section}{\numberline {3}Lista de componentes}{7}{section.3}%
\contentsline {section}{\numberline {4}Diseño}{8}{section.4}%
\contentsline {subsection}{\numberline {4.a}Condicionador de voltaje}{8}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.b}Switches de selección}{9}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.c}LEDs de advertencia}{10}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.d}Diseño del software}{11}{subsection.4.4}%
\contentsline {section}{\numberline {5}Análisis de resultados}{13}{section.5}%
\contentsline {section}{\numberline {6}Conclusiones y recomendaciones}{17}{section.6}%
