\input{preamble}

\pagenumbering{roman}

\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\title{
{
    \begin{tikzpicture}[overlay, remember picture]
        \node[anchor=north west, %anchor is upper left corner of the graphic
            xshift=3cm, %shifting around
            yshift=-3.45cm] 
            at (current page.north west) %left upper corner of the page
        {\includegraphics[height=1.9cm]{img/EIE_v.png}}; 
    \end{tikzpicture}
    \begin{tikzpicture}[overlay, remember picture]
        \node[anchor=north east, %anchor is upper left corner of the graphic
            xshift=-2.5cm, %shifting around
            yshift=-3.65cm] 
            at (current page.north east) %left upper corner of the page
        {\includegraphics[height=1.4cm]{img/ucr.png}}; 
    \end{tikzpicture}
    \Large 
        \textbf{Universidad de Costa Rica}\\
        Facultad de Ingeniería\\
        Escuela de Ingeniería Eléctrica\\~\\
        \vspace{3cm} \texttt{IE-0624} Laboratorio de Microcontroladores
    }
    ~\\~\\\vspace{3cm} 
    {\textbf{\LARGE Laboratorio 3}}\\
    \vspace{3cm}
}

\pagestyle{fancy}
\fancyhf{}
\lhead{IE-0624: Laboratorio de Microcontroladores}
\chead{}
\rhead{Laboratorio 3}
\lfoot{Escuela de Ingeniería Eléctrica}
% \cfoot{\thepage\ de \pageref{LastPage}}
\cfoot{\thepage}
\rfoot{Universidad de Costa Rica}

\author{Victor Manuel Yeom Song - \texttt{B78494}
    }

\date{II-2020}

\makeatletter
\g@addto@macro{\UrlBreaks}{\UrlOrds}
\makeatother
\begin{document}

\maketitle

\newpage
\tableofcontents

\newpage
\pagenumbering{arabic}

\section{Resumen}

En este trabajo se presenta la implementación de un voltímetro de 4 canales con un Arduino UNO, convertidores de voltaje (compuestos por resistencias y amplificadores operacionales), switches de control de medición (para diferenciar entre AC y DC, así como decidir si se realiza comunicación por USART), 4 fuentes de prueba y un sistema de LEDs de alerta por si el valor absoluto de la tensión en alguno de los canales sobrepasa los \SI{12}{V}. 

Se realizó una comunicación serial entre el Arduino y la computadora, mediante comunicación por medio de USART y el comando \texttt{socat} en Linux. Mediante un script de Python se logró leer la información enviada por el Arduino UNO a la computadora, y registrar la misma en un archivo de formato CSV.

Se concluyó que la placa Arduino UNO provee una interfaz de programación poderosa para el usuario, dado que al poder adaptar una mayor abstracción al hardware permite el desarrollo de programas más complejos sin mayor dificultad. A su vez, las capacidades de comunicación de dicha placa con una computadora solo profundiza el poder que puede tener, dado que por medio de la comunicación de datos y el poder computacional de ambos dispositivos combinados permite una amplia gama de aplicaciones.



\newpage
\section{Nota teórica}

\subsection{El microcontrolador ATmega328/P}

La tarjeta Arduino UNO emplea el microcontrolador ATmega328/P, el cual es de tecnología CMOS de 8 bits, bajo consumo de potencia y se basa en la arquitectura AVR RISC. Las características de este microcontrolador se describen en \cite{atmega}:

\begin{itemize}
    \item Tensión de operación de \SI{1,8}{V} a \SI{5,5}{V}.
    \item Rango de operación de temperatura de \SI{-40}{\celsius} a \SI{105}{\celsius}.
    \item Velocidad de operación máxima de \SI{20}{MHz} con una tensión mayor a \SI{4,5}{V}.
    \item Arquitectura RISC con 131 operaciones, la mayoría ejecutadas en un solo ciclo de reloj. Cuenta con 32 registros de propósito general, operación estática y un multiplicador de 2 ciclos.
    \item Dos timers de 8 bits con prescaler y modo de comparación y uno más de 16 bits con los mismos modos y modo de captura. Cuenta con seis canales PWM, un comparador analógico, un timer tipo watchdog con oscilador, USI, USART y una interfaz serial de 2 cables.
    \item 32 líneas de I/O programables, 28 pines PDIP y pad QFN/MLF de 28 y 32.
\end{itemize}

El diagrama de pinout del microcontrolador se muestra en la figura \ref{fig:mcu_pinout}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .6\linewidth]{img/background/mcu_pinout.png}
    \caption{Diagrama de pinout del ATMega328/P. Recuperado de \cite{atmega}.}
    \label{fig:mcu_pinout}
\end{figure}

La funcionalidad de los pines se describen a continuación:

\begin{itemize}
    \item Pines de alimentación VCC y GND.
    \item Puerto B: El puerto cuenta con 8 pines de I/O bidireccionales, con resistencias de pull-up. Los buffers en la salida tienen características de control simétricas con alta capacidad de fuente y drenaje.
    \item Puerto C: El puerto cuenta con 6 pines de I/O bidireccionales, con resistencias de pull-up. Los buffers en la salida tienen características de control simétricas con alta capacidad de fuente y drenaje.
    \item PC6/Reset: Si el fusible RSTDISBL está programado, PC6 se utiliza como puerto de I/O, caso contrario se utiliza como Reset. Si está en bajo por más tiempo de la duración mínima de pulso se genera un reset, aunque no haya un reloj corriendo.
    \item Puerto D: El puerto cuenta con 8 pines de I/O bidireccionales, con resistencias de pull-up. Los buffers en la salida tienen características de control simétricas con alta capacidad de fuente y drenaje.
    \item AVcc: Tensión de alimentación del convertidor analógico-digital, conformado por PC[3:0] y PE[3:2]. Debe estar conectado de forma externa a Vcc aunque no se use el convertidor. Si se utiliza, se debe conectar a Vcc con un filtro pasa-bajo.
    \item AREF: Pin de referencia analógico para el convertidor analógico-digital.
\end{itemize}

\subsection{Arduino UNO}
El Arduino UNO es una placa open source manejada por el ATmega328/P. Dicha placa funciona como interfaz para el microcontrolador, facilitando su conexión a circuitos externos y su programación. La placa facilita la comunicación con la computadora por medio de un puerto USB.

El diagrama de pinout del Arduino UNO se presenta en la figura \ref{fig:arduino_pinout}, donde se resaltan 14 pines de I/O digital desde D0 hasta D13 y 6 pines de I/O analógica desde A0 hasta A5.

\begin{figure}[H]
    \centering
    \includegraphics[width = .7\linewidth]{img/background/arduino_pinout.png}
    \caption{Diagrama de pinout del Arduino UNO REV3. Recuperado de \cite{ard_pinout}}
    \label{fig:arduino_pinout}
\end{figure}

De especial interés para este laboratorio, se tienen las siguientes funciones disponibles en el entorno de programación de Arduino:

\begin{itemize}
    \item pinMode(): Recibe dos parámetros: un pin y un indicador de entrada/salida. Configura el pin como entrada si recibe INPUT o como salida si recibe OUTPUT.
    \item digitalWrite(): Recibe dos parámetros: un pin y un indicador de alto/bajo. Pone en HIGH o LOW el pin digital que recibe dependiendo del segundo parámetro.
    \item digitalRead(): Devuelve el valor leído del pin digital que recibe como parámetro. Puede ser LOW o HIGH.
    \item analogRead(): Devuelve el valor leído del pin analógico que recibe como parámetro. Es un valor entre 0 y 1023 correspondiente a un mapeo de \SI{0}{V} a \SI{5}{v} en 10 bits, según el convertidor analógico-digital del microcontrolador.
\end{itemize}

\subsection{Pantalla PCD8544}

En \cite{screen} se indica que el PCD8544 es un controlador de baja potencia que maneja una pantalla 48x84 (48 filas y 84 columnas). Todas las funciones necesarias para controlar la pantalla se proporcionan en un solo chip, de modo que se reduce la necesidad de componentes externos y se reduce el consumo de potencia. Consume hasta un máximo de \SI{50}{mA}.

La pantalla se controla mediante los siguientes pines:

\begin{itemize}
    \item Reset: Reinicia el módulo. Se activa en un 0 lógico.
    \item Chip enable (CE/SCE): Habilita el módulo para leer datos. Es activado en 0 lógico.
    \item Data/Command (DC o D/C): Elige la entrada de comandos o direcciones (LOW) o entrada de datos (HIGH).
    \item Serial Data Line (SDIN): Entrada de datos seriales.
    \item Serial Clock Line (CLK): Entrada para señal de reloj, desde 0 hasta 4 Mbit/s.
    \item Power (Vcc): Alimentación del componente, desde \SI{2,7}{V} hasta \SI{3,3}{V}.
    \item Back Light (BL): Alimentación de la luz trasera de la pantalla.
    \item Ground (GND): Conexión a tierra.
\end{itemize}

Para manejar la pantalla con el Arduino, simplemente se utiliza una biblioteca para facilitar la comunicación entre la placa y el módulo. La biblioteca utilizada para este laboratorio es la que se provee en \url{https://github.com/carlosefr/pcd8544}. Las conexiones entre la pantalla y el Arduino UNO se describen según el cuadro \ref{tab:screenPinsArd}.

\begin{table}[H]
    \centering
    \begin{tabular}{cc}
    \toprule
        Pin de la pantalla & Pin del Arduino UNO \\ \midrule
        CLK & D3 \\
        SDIN & D4 \\
        DC & D5 \\
        Reset & D6 \\
        CE & D7 \\ \bottomrule
    \end{tabular}
    \caption{Conexiones entre el módulo y el Arduino UNO a realizar para la programación de la pantalla}
    \label{tab:screenPinsArd}
\end{table}

Las funciones de la biblioteca utilizadas para transmitir información a la pantalla son:

\begin{itemize}
    \item begin(): Inicializa el display de la pantalla. Recibe dos parámetros: 84 y 48, dimensiones que caracterizan la pantalla utilizada.
    \item print(): Despliega en la pantalla símbolos ASCII de datos pasados por parámetro. 
    \item setCursor(): Define la posición del cursor.
\end{itemize}

\subsection{Convertidor Analógico-Digital (ADC)}

El ADC convierte una señal analógica recibida a una representación dada en el dominio digital. Para esto, se realiza un muestreo de la primera que mapea valores analógicos a valores digitales (definidos por una cierta combinación de bits). Un ejemplo de esto se muestra en la figura \ref{fig:adc3bit}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .5\linewidth]{img/background/adc.png}
    \caption{Esquema de conversión analógico-digital de 8 niveles. Recuperado de \url{https://en.wikipedia.org/wiki/Analog-to-digital_converter}.}
    \label{fig:adc3bit}
\end{figure}

El ATmega328/P realiza una conversión analógico-digital de 10 bits; lee valores analógicos de \SI{0}{V} a \SI{5}{V} y los ``traduce'' a valores de 0 a 1023. Por ejemplo, si lee \SI{5}{V}, se lee el valor como un 1023. En \cite{atmega} se detalla la conversión con \eqref{eqn:adc_atmega}.

\begin{equation}
    \label{eqn:adc_atmega}
    \text{ADC} = \frac{V_{in} \cdot 1023}{V_{ref}}
\end{equation}

Donde:

\begin{itemize}
    \item ADC: Valor discreto que representa el valor analógico convertido a un valor digital.
    \item $V_{in}$: Tensión de entrada medida.
    \item $V_{ref}$: Tensión de referencia seleccionada, la cual en este laboratorio corresponde a \SI{5}{V}.
\end{itemize}

\subsection{Comunicación serial USART}

El ATmega328/P contiene un periférico USART (Universal Synchronous/Asynchronous Receiver Transmitter), el cual se utiliza para la comunicación con datos seriales. En \cite{usart} se indica que esto se realiza mediante la conexión directa de dos puertos USART; el transmisor convierte la información paralela del bus de entrada a una forma serial y se la envía al receptor, el cual se encarga de ``des-serializar'' los datos para la lectura. La transmisión de datos se puede realizar tanto sincrónicamente como asincrónicamente.

Para manejar datos serializados con el Arduino UNO, se utilizan las siguientes funciones:

\begin{itemize}
    \item Serial.begin(): Inicializa una comunicación serial y define el baud rate.
    \item Serial.print(): Envía datos de cualquier tipo, los cuales recibe como parámetros.
\end{itemize}

\subsection{El diodo semiconductor}

Los diodos semiconductores son dispositivos que permiten el flujo de la corriente en una dirección pero no en el otro. En \cite{Boylestad} se indica que son creados a partir de la unión de un material tipo p (silicio dopado con algún elemento como el boro) con otro tipo n (silicio dopado con algún elemento como el fósforo). También se indica que si uno deseara un LED, simplemente se agregan materiales al silicio o el encapsulado que reaccionan a los campos eléctricos y generan luz.

La ecuación que describe el comportamiento del diodo es dada por \eqref{eqn:diode}:

\begin{equation} \label{eqn:diode}
    I_D=I_s \left (e^{\frac{V_D}{V_T}}-1 \right)
\end{equation}

Donde $I_D$ es la corriente a través del diodo, $I_S$ es la corriente de saturación inversa, $V_D$ la tensión a través del diodo y $V_T$ el voltaje térmico, determinado por \eqref{eqn:threshold}:

\begin{equation}\label{eqn:threshold}
    V_T=\frac{\kappa T}{q}
\end{equation}

Donde $\kappa$ es la constante de Boltzmann, T la temperatura en kelvins y q la carga del electrón.

\subsection{Rebote de switches}

En \cite{Bounce} se indica que en la vida real, cuando se presiona un botón o switch, se produce el contacto de dos partes de metal. Este contacto puede parecer instantáneo, pero durante el momento de activación del botón se pueden dar varios contactos pequeños antes del cierre total del botón o switch, por lo que las señales eléctricas que produce el switch ``rebotan'' entre apagado y encendido en los momentos previos al cierre total del mismo. Se pueden producir entre 10 y 100 de estos rebotes en \SI{1}{ms}, lo cual resulta problemático dado que el hardware utilizado en la actualidad puede operar a velocidades más altas, por lo que puede interpretar esos rebotes como varias activaciones del switch.

Para lidiar con estos rebotes, generalmente se utilizan capacitores para que filtren estas oscilaciones. Para el laboratorio, además de un capacitor, se utiliza una resistencia para limitar la corriente que se puede producir a la entrada del microcontrolador, así como facilitar la carga y descarga segura del capacitor cuando se presiona el botón. La adición de estos elementos no produciría un efecto muy visible en la simulación en la herramienta SimulIDE, dado que los switches en la misma son ideales y no producen rebote, pero se toman en cuenta para una eventual implementación en la vida real.

\newpage
\section{Lista de componentes}

La lista de componentes necesarios para desarrollar el diseño se presentan en el cuadro \ref{tab:components}.

\begin{table}[H]
    \centering
    \begin{tabular}{cccc}
        \toprule
        Componente & Valor nominal & Precio & Cantidad \\ \midrule
        Resistencia \SI{1/4}{W} & \SI{100}{k\ohm} & \$5,53 / 10 piezas & 2 \\ \hline
        Resistencia \SI{1/4}{W} & \SI{1}{k\ohm} & \$5,49 / 10 piezas & 4 \\ \hline
        Resistencia \SI{1/8}{W} & \SI{1,6}{k\ohm} & \$4,25 / 20 piezas & 4 \\ \hline
        Resistencia \SI{1/4}{W} & \SI{24}{k\ohm} & \$2,52 / 10 piezas & 8 \\ \hline
        Resistencia \SI{5}{W} & \SI{5}{k\ohm} & \$2,9 / 10 piezas & 8 \\ \hline
        Capacitor cerámico & \SI{100}{nF} & \$6,99 / 50 piezas & 6 \\\hline
        \multirow{2}{*}{LED TLLK4401} & $V_{fw} = \SI{1,8}{V}$ & \multirow{2}{*}{\$3,09 / 10 piezas} & \multirow{2}{*}{4} \\
        &  $I_{fw} = \SI{2}{mA}$ & & \\\hline
        Amplificador OPA377 & --- & \$1,04 / 1 pieza & 4 \\ \hline
        Pantalla PCD8544 & --- & \$8,79 / 1 pieza & 1 \\ \hline
        Arduino UNO Rev3 & --- & \$23 / 1 unidad & 1 \\ \bottomrule
    \end{tabular}
    \caption{Lista de componentes para realizar el diseño.}
    \label{tab:components}
\end{table}

La información sobre los costos y aspectos de los componentes se obtuvieron en las siguientes direcciones URL:

\begin{itemize}
    \item Resistencia de \SI{100}{k\ohm}: \newline \url{https://www.amazon.com/Projects-100k-Resistors-Choose-Quantity/dp/B071L6BDJV}
    
    \item Resistencia de \SI{1}{k\ohm}: \newline \url{https://www.amazon.com/Watt-Carbon-Film-Resistors-pack/dp/B00EV2QC96}
    
    \item Resistencia de \SI{1,6}{k\ohm}: \newline \url{https://www.ebay.com/itm/1-6-K-1-6K-1600-Ohm-1-8-Watt-Resistor-universal-Pack-of-20-/390406343303}
    
    \item Resistencia de \SI{24}{k\ohm}: \newline \url{https://www.amazon.com/RESISTOR-CARBON-FILM-24K-WATT/dp/B00CHTM848}
    
    \item Resistencia de \SI{5}{k\ohm}: \newline \url{https://www.jameco.com/z/RSF-MO5WS5KJBU-Jameco-Valuepro-Resistor-Metal-Oxide-5K-Ohm-5-Watt-5-_2274335.html}
    
    \item LED rojo: \url{TLLK4401}
    
    \item Capacitor cerámico: \url{https://www.amazon.com/BOJACK-Capacitors-Low-Voltage-Dielectric-Capacitor/dp/B07X5XTDPB}
    
    \item Amplificador operacional: \url{https://www.ti.com/product/OPA377#order-quality}
    
    \item Pantalla LCD: \url{https://www.geekbuying.com/item/1-6-Nokia-5110-PCD8544-LCD-Display-Module-With-White-Backlight-For-Raspberry-Pi-B--B-343822.html}
    
    \item Arduino UNO Rev3: \url{https://store.arduino.cc/usa/arduino-uno-rev3}
\end{itemize}

\newpage
\section{Diseño}

El circuito diseñado final es mostrado en la figura \ref{fig:circDesign}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .8\linewidth]{img/design/circDesign.png}
    \caption{Diseño del circuito implementado. Elaboración propia.}
    \label{fig:circDesign}
\end{figure}

\subsection{Condicionador de voltaje}

Para el condicionador de voltaje, se apoyó en el diseño disponible en \url{https://www.daycounter.com/Circuits/OpAmp-Level-Shifter/OpAmp-Level-Shifter.phtml}, basado en un convertidor de nivel sin inversión. El circuito asociado a dicho diseño se presenta en la figura \ref{fig:converter}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .6\linewidth]{img/design/converter.png}
    \caption{Circuito convertidor de nivel sin inversión, para conversión de 10 Vpp a 3,3 V.}
    \label{fig:converter}
\end{figure}

Para dicho diseño, se indica que la ganancia a la salida del amplificador operacional es dada por:

\begin{equation*}
    A = \frac{R4}{R1} \cdot \frac{R1 + R2}{R3 + R4}
\end{equation*}

Por facilidad de diseño, se asume que R1=R3 y R2=R4, con lo que se obtendría que la ecuación de la ganancia para el amplificador es dada por \eqref{eqn:opampGain}.

\begin{equation}
    \label{eqn:opampGain}
    A = \frac{R4}{R1}
\end{equation}

Se ocupa convertir de una señal de (posiblemente) 24 Vpp a una de \SI{5}{V}, por lo que se desea una ganancia de $\frac{5}{24}$, de modo que se escogen unos valores de resistencia para R4 y R1 de \SI{5}{k\ohm} y \SI{24}{k\ohm} respectivamente. Luego, se indica que la ganancia del offset introducido en la entrada positiva del amplificador es dada por \eqref{eqn:offsetGain}.

\begin{equation}
    \label{eqn:offsetGain}
    \frac{R3}{R1}
\end{equation}

Pero por diseño se tiene que R1=R3, por lo que la ganancia del offset es de 1. Así, se alimenta el amplificador con \SI{5}{V} (en lugar de 3,3 V en la figura \ref{fig:converter}) y se introduce una tensión de \SI{2,5}{V} en la entrada positiva para desplazar la salida del amplificador operacional a valores entre \SI{0}{V} y \SI{5}{V}.

Se conservan los valores de capacitancia a la salida del circuito y se coloca una resistencia de salida de \SI{1}{k\ohm}, lo cual produce una constante de carga de $\tau = \SI{0,1}{ms}$ y una corriente de carga/descarga máxima de \SI{5}{mA}, lo cual produciría en el peor caso una corriente de entrada de \SI{20}{mA} con los 4 canales de entrada del voltímetro. Se pueden cambiar estos valores posteriormente para poder tener tiempos de respuesta más rápidos o limitar más la corriente, pero se debe tener cuidado de no sobrepasar el límite de corriente de entrada del Arduino UNO. Al final, por canal se tiene un circuito similar al de la figura \ref{fig:canal1}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .7\linewidth]{img/design/canal1.png}
    \caption{Circuito para conversión de 24 Vpp a 5 V. Elaboración propia.}
    \label{fig:canal1}
\end{figure}

\subsection{Switches de selección}

Se tiene el circuito de la figura \ref{fig:switch} para los switches de selección descritos en el enunciado. La configuración de la resistencia y el capacitor indica una constante de tiempo $\tau = \SI{10}{ms}$ y una corriente de carga/descarga máxima de \SI{50}{\micro A}. Esta corriente es bastante pequeña, por lo que en conjunto con las corrientes de entrada de cada canal de medición se asegura que se cumplen las condiciones de alimentación del Arduino UNO y no se darían problemas por sobrecorrientes.

\begin{figure}[H]
    \centering
    \includegraphics[width = .4\linewidth]{img/design/switch.png}
    \caption{Circuito para los switches de selección de modo. Elaboración propia.}
    \label{fig:switch}
\end{figure}

\subsection{LEDs de advertencia}

Se plantea un diseño con ``ramas'' de LEDs conectadas al microcontrolador, con cada rama correspondiendo a un LED de advertencia.

Cada rama debe tener una resistencia asociada que limite la corriente que corre por los LEDs (con corriente de \textit{forward} máxima de \SI{30}{mA} como se indica en \cite{LED}), así como limitar la corriente que consumen dado que el Arduino UNO tiene una corriente de suministro máxima de \SI{100}{mA} en total a lo largo de las salidas GPIO, y se asume un peor caso en el que la pantalla LCD consume \SI{50}{mA}.

Se tiene la rama de una resistencia y un LED, dada en la figura \ref{fig:designR_1LED}:

\begin{figure}[H]
    \centering
    \includegraphics[width = .4\linewidth]{img/design/designR_1LED.png}
    \caption{Circuito de diseño para la rama de 1 LED. Elaboración propia.}
    \label{fig:designR_1LED}
\end{figure}

Nótese que aquí se toma el modelo simplificado del diodo y no se toman en cuenta sus propiedades resistivas y capacitivas, lo cual puede inducir un error en los resultados finales. Estos aspectos físicos no se toman en cuenta dado que pueden variar mucho entre diodo y diodo, no se indican en \cite{LED} y la relación entre corriente y tensión no es lineal, como se modela por la ecuación del diodo de Shockley.

Se define una corriente $I_{D} = \SI{2}{mA}$, que es la corriente de alimentación especificada del LED \cite{LED}, para diseñar la resistencia R. Por una LTK, se tiene:

\begin{eqnarray*}
    I_{D}R + 1,8 &=& 5\\
    \iff R &=& \frac{5-1,8}{I_{D}}\\
    \iff R &=& \SI{1600}{\ohm}
\end{eqnarray*}

Así, con los 4 LEDs encendidos se entregaría un máximo de \SI{8}{mA} a lo largo de los 4 diodos de advertencia, por lo que en conjunto con un máximo de \SI{50}{mA} que podría ir a la pantalla, se asegura que se mantiene una salida de corriente menor a \SI{100}{mA} permitida para el Arduino UNO.

\subsection{Diseño del software}

El diseño del programa principal que se ejecuta en el Arduino UNO se resume en la figura \ref{fig:mainDiag}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .7\linewidth]{img/design/mainDiag.jpg}
    \caption{Diseño del programa principal para procesar las señales. Elaboración propia.}
    \label{fig:mainDiag}
\end{figure}

Cabe resaltar que para el diseño, se asume que las señales oscilatorias que entran en los canales son sinusoidales, dado que el cálculo del valor RMS se realiza mediante la multiplicación de 0.707 (una aproximación de una división entre raíz de 2). La amplitud de una señal AC se obtiene como la mitad del valor pico-pico medido, por lo que también se asume una señal que es simétrica alrededor de un eje horizontal.

Si la señal fuera puramente DC, el valor máximo y el mínimo son iguales y su promedio da igual a la señal medida. Por otro lado, si la señal es mixta (es decir, una señal sinusoidal con componente DC), el promedio del mínimo y del máximo da el componente DC dada la asunción de simetría de la señal.

Luego, para comunicaciones seriales se utilizó la utilidad \texttt{socat} y el comando de terminal \texttt{socat PTY,link=/tmp/ttyS0,raw,echo=0 PTY,link=/tmp/ttyS1,raw,echo=0} visto en clase. Esto permite la conexión serial entre el Arduino simulado en SimulIDE y la computadora. El programa realizado en Python tiene la estructura del diagrama de flujo mostrado en la figura \ref{fig:usartDiag}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .6\linewidth]{img/design/usartDiag.jpg}
    \caption{Diagrama de flujo para la comunicación serial con Python. Elaboración propia.}
    \label{fig:usartDiag}
\end{figure}

\newpage
\section{Análisis de resultados}

Se realiza una prueba con las siguientes señales en los canales:

\begin{itemize}
    \item V1 = $-2 + 2\sen(3\pi t)$ V
    \item V2 = -12 V
    \item V3 = 0 V
    \item V4 = 12 V
\end{itemize}

De esta manera, el programa debe leer que en los canales 2 a 4 hay una componente AC de \SI{0}{V} y en el canal 1 un componente AC de \SI{1,41}{Vrms}, mientras que el componente DC en los canales 1 a 4 son de \SI{-2}{V}, \SI{-12}{V}, \SI{0}{V} y \SI{12}{V}, en ese orden. Así, se deben encender únicamente los LEDs de los canales 2 y 4.

Primero se verifica la funcionalidad de los convertidores de nivel. Se tiene la figura \ref{fig:AC_conv}, donde se ve que a la salida del convertidor hay una señal sinusoidal, en fase con la original, con valor mayor o igual que 0 en todo momento y esta obedece el margen de amplitud (que debe ser menor a \SI{5}{V}). Esto valida el diseño del convertidor de nivel.

\begin{figure}[H]
    \centering
    \includegraphics[width = .8\linewidth]{img/results/result_AC_conv.png}
    \caption{Resultado del convertidor de nivel para la señal del canal 1. Elaboración propia.}
    \label{fig:AC_conv}
\end{figure}

Luego, para la lectura de los valores se presentan los resultados mostrados en la pantalla. En la figura \ref{fig:LCD-dc} se presentan los valores leídos de componente DC en cada canal (con el switch de lectura AC/DC para mostrar su funcionalidad).

\begin{figure}[H]
    \centering
    \includegraphics[width = .6\linewidth]{img/results/result_DC_screen.png}
    \caption{Lectura de valores DC en la pantalla LCD. Elaboración propia.}
    \label{fig:LCD-dc}
\end{figure}

Se puede notar que los valores mayores a \SI{-12}{V} no tienen el valor exacto que se introdujo teóricamente; esto se puede deber a un error de muestreo, así como error producido por los algoritmos de redondeo que se producen en la herramienta SimulIDE. Como el error no es grande, se considera que se logra leer bien el componente DC en cada canal.

Luego, para la lectura en la pantalla de los valores AC se tiene la figura \ref{fig:LCD-ac}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .6\linewidth]{img/results/result_AC_screen.png}
    \caption{Lectura de valores AC en la pantalla LCD. Elaboración propia.}
    \label{fig:LCD-ac}
\end{figure}

Aquí, se puede ver que el valor obtenido del RMS de la señal sinusoidal es correcto (con un poco de error), dado que el valor RMS con una amplitud de \SI{2}{V} sería de aproximadamente \SI{1,414}{V}. El error producido, otra vez, puede ser por error de muestreo y redondeos realizados por el simulador.

Luego, se ve la funcionalidad de los LEDs de advertencia. Esto se presenta en la figura \ref{fig:LEDsRes}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .3\linewidth]{img/results/result_LEDs.png}
    \caption{Resultado de los LEDs de advertencia con la prueba realizada. Elaboración propia.}
    \label{fig:LEDsRes}
\end{figure}

Aquí, se puede ver que como se esperaba, los únicos LEDs de advertencia encendidos son los asociados a los canales 2 y 4. Por esto, se valida su funcionamiento.

Ahora, para la comunicación serial se verifica el funcionamiento del switch mediante el monitor serial del Arduino (así como la terminal de Linux, cuando se reciben datos con Python). Los datos se envían con formato:

\begin{center}
    Canal, Componente DC, Componente AC
\end{center}

Cuando el switch está abierto (es decir, no se desea transmitir datos), se tienen la\ref{fig:monitor_switchOFF} y \ref{fig:python_switchOFF}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .4\linewidth]{img/results/serialSw_OFF.png}
    \caption{Monitor de comunicación serial con el switch abierto. Elaboración propia.}
    \label{fig:monitor_switchOFF}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width = .6\linewidth]{img/results/usartPyOFF.png}
    \caption{Salida de Python por la comunicación serial con el switch abierto. Elaboración propia.}
    \label{fig:python_switchOFF}
\end{figure}

Como es de esperarse, no se imprime nada en ninguna de las pantallas. Ahora, con el switch cerrado, se tienen las figuras \ref{fig:monitor_switchON} y \ref{fig:python_switchON}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .4\linewidth]{img/results/serialSw_ON.png}
    \caption{Monitor de comunicación serial con el switch cerrado. Elaboración propia.}
    \label{fig:monitor_switchON}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width = .6\linewidth]{img/results/usartPyON.png}
    \caption{Salida de Python por la comunicación serial con el switch cerrado. Elaboración propia.}
    \label{fig:python_switchON}
\end{figure}

Así, se ve que la comunicación serial se está dando de forma deseada. Se verifica que los datos recibidos por la comunicación serial coinciden con los que se mostraron en la pantalla LCD. 

\newpage
Finalmente, para la escritura de los datos en un archivo CSV, se corrobora la escritura de los datos obtenidos con la figura \ref{fig:csvResult}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .5\linewidth]{img/results/csvResult.png}
    \caption{CSV resultante de la comunicación serial. Elaboración propia.}
    \label{fig:csvResult}
\end{figure}

En este archivo se confirma que la escritura de los datos en general es consistente, y los datos escritos en el archivo coinciden con los datos observados en el monitor serial y la pantalla LCD.

Con esto, se verifica la funcionalidad general del diseño implementado y se determina que funciona de forma deseada.

\section{Conclusiones y recomendaciones}

La placa Arduino UNO le da una cantidad impresionante de poder al programador, dado que permite una mayor cantidad de abstracciones que la que se pudo emplear en trabajos anteriores. Aunado al hecho de que no se programan las instrucciones directamente a los registros y a la flexibilidad de las herramientas presentes para la plataforma, la placa facilita en gran manera el proceso de programar los proyectos que uno desea realizar.

Se observó la utilidad de los convertidores de nivel en el mundo digital, dado que permiten utilizar sistemas digitales relativamente frágiles para medir e interfazarse con sistemas de mayor potencia. Esto permite un campo de trabajo sumamente amplio con los circuitos digitales, siempre y cuando se pueda realizar una ``traducción'' entre los sistemas que se comunican.

Se logró realizar una comunicación serial entre el Arduino y la computadora, esto registrado en un archivo de formato CSV. Esto es útil para poder realizar aplicaciones de mediciones remotas (por ejemplo, se puede conectar el Arduino a una Raspberry Pi y que registre los datos de ciertas medidas en el Pi), así como el intercambio de datos entre distintos sistemas.

% \bibliography{annot}

\begin{thebibliography}{IEEEannot}

\bibitem{atmega} Atmel Corporation. (2016). \textit{8-bit AVR Microcontrollers ATmega328/P}.

\bibitem{screen} Philips Semiconductors. (1999). \textit{PCD8544 48 x 84 pixels matrix LCD controller/driver}.

\bibitem{ard_pinout} Arduino. \textit{ARDUINO UNO REV3 pinout}.

\bibitem{usart} Jacob Beningo. (2015). \textit{USART vs UART: Know the difference}. Disponible en: \url{https://www.edn.com/usart-vs-uart-know-the-difference/}.

\bibitem{Bounce} Jens Christoffersen. ``Switch Bounce and How to Deal with It'', 2015. [En línea]. Disponible en: \url{https://www.allaboutcircuits.com/technical-articles/switch-bounce-how-to-deal-with-it/}

\bibitem{Boylestad} Robert L. Boylestad \& Louis Nashelsky, \textit{Electrónica: Teoría de Circuitos y Dispositivos Electrónicos}. Pearson Education, 2009.

\bibitem{LED} Vishay Semiconductors, ``High Intensity LED in $\varnothing$ \SI{3}{mm} Tinted Diffused Package'', 2014. [En línea]. Disponible en: \url{https://www.vishay.com/docs/83343/tlle4401.pdf}

\end{thebibliography}


\end{document}
