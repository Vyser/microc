Laboratorio 3
==========
--------------------
Estudiante:
--------------------
+ Victor Manuel Yeom Song - B78494

En este repositorio se encuentra el trabajo realizado para el Laboratorio 3 del curso IE-0624 Laboratorio de Microcontroladores. 

En el directorio src se hallan el código fuente y los archivos de simulación necesarios para la simulación del diseño en SimulIDE. El archivo volt.simu contiene el circuito implementado en SimulIDE y el archivo volt.ino el código implementado en la plataforma de Arduino para la funcionalidad del Arduino UNO.

Para poder realizar la simulación, se debe compilar el archivo volt.ino (el Makefile de src se encarga de esto) de donde se obtiene un archivo de extensión .hex, llamado "src_.hex", que se halla dentro de la carpeta build-uno. Luego, con el archivo volt.simu abierto en SimulIDE, se debe cargar el *firmware* al microcontrolador. Este firmware corresponde al archivo src_.hex.

Para la comunicación serial entre el Arduino y la computadora, se debe ejecutar el comando socat PTY,link=/tmp/ttyS0,raw,echo=0 PTY,link=/tmp/ttyS1,raw,echo=0 para habilitar la comunicación serial entre el Arduino UNO de la simulación y la computadora. Una vez que se habilita, dentro del diagrama del circuito, se debe encender el puerto UART asociado a la placa.

Para ejecutar el script de Python, se necesitan las bibliotecas *serial* y *csv*, las cuales se encargan de lectura de datos seriales y manejo de archivos CSV respectivamente. Cuando se haya ejecutado el comando socat y habilitado la comunicación serial en la simulación, simplemente se debe ejecutar el comando:

python usart.py

dentro de la carpeta src para iniciar la transmisión de datos.

En el directorio informe se hallan los archivos necesarios para generar el informe del laboratorio. Se incluye un Makefile que compila los archivos y abre el documento pdf generado.