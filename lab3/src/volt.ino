#include "./pcd8544-master/PCD8544.h"
#include "./pcd8544-master/PCD8544.cpp"

// Se manejan los puertos de medicion por medio de un arreglo por facilidad
int inputs[] = {A0, A1, A2, A3};

// Puertos de salida para los LEDs de advertencia
int LEDs[] = {11, 10, 9, 8};

static PCD8544 lcd;

// Arreglos para manejar valores minimos y maximos para los calculos de AC y DC
int min[4];
int max[4];

// Arreglos que manejan los componentes de AC y DC en cada puerto
double ac[4];
double dc[4];

// Ganancia de un op-amp para poder recuperar la medicion original
float gain = 5/24.0;

// Funciones para el calculo de componentes AC y DC de las señales de entrada
void calc_dc(void);
void calc_ac(void);

// Funcion para impresion en pantalla LCD
void print_screen(void);

// Funcion para transmision de datos por USART
void sendSerial(void);

// Funcion para revision de LEDs
void checkVolt(void);

/* SETUP */
void setup() {
	
	// Stream de datos serial
	Serial.begin(9600);

    // Resolucion de la pantalla
	lcd.begin(84, 48);

    // INPUTS
	pinMode(inputs[0], INPUT); // Medicion 1
	pinMode(inputs[1], INPUT);	// Medicion 2
	pinMode(inputs[2], INPUT);	// Medicion 3
	pinMode(inputs[3], INPUT); // Medicion 4
    pinMode(A4, INPUT); // Formato de medicion
    pinMode(A5, INPUT); // Envio de datos seriales

    // OUTPUTS
    pinMode(LEDs[0], OUTPUT); // LED canal 1
    pinMode(LEDs[1], OUTPUT); // LED canal 2
    pinMode(LEDs[2], OUTPUT); // LED canal 3
    pinMode(LEDs[3], OUTPUT); // LED canal 4
}

void loop() {
	unsigned long beginning = millis(); // Medida para poder registrar un tiempo de muestreo

    // Se asumen los "peores" casos de maximo y minimo en cada canal, para poder medir el minimo y maximo en cada uno
    for (short i = 0; i < 4; i++){
		max[i] = 0;
		min[i] = 1024;
	}

	// Medio segundo de muestreo
	while ((millis() - beginning) <= 1000){
		// Muestreo de valores minimo y maximo en medio segundo
		for (short i = 0; i < 4; i ++){
			int measure = analogRead(inputs[i]);
            max[i] = (measure > max[i]) ? measure : max[i];
            min[i] = (measure < min[i]) ? measure : min[i];
		}
	}

	// Se miden los valores de amplitud y componente DC de cada señal medida
    calc_dc();
	calc_ac();
    print_screen();
    if (digitalRead(A5) == HIGH){
        sendSerial();
    }
    checkVolt();
}

void calc_dc(){
    for (short i = 0; i < 4; i++){
		float analog_min = min[i]*5/1024.0;
		float analog_max = max[i]*5/1024.0;
		dc[i] = (analog_max + analog_min)/(2*gain) - 12.0;
	}
}

void calc_ac(){
    for (short i = 0; i < 4; i++){
        float analog_min = min[i]*5/1024.0;
		float analog_max = max[i]*5/1024.0;
		ac[i] = ( (analog_max - analog_min) / (2*gain) )*0.707;
	}
}

void print_screen(){
    lcd.clear();
    if (digitalRead(A4) == HIGH){
        lcd.setCursor(0, 0);
	    lcd.print("DC (V)");
	    lcd.setCursor(0, 2);
	    lcd.print("V1: ");
	    lcd.print(dc[0]);
	    lcd.setCursor(0, 3);
	    lcd.print("V2: ");
	    lcd.print(dc[1]);
	    lcd.setCursor(0, 4);
	    lcd.print("V3: ");
	    lcd.print(dc[2]);
	    lcd.setCursor(0, 5);
	    lcd.print("V4: ");
	    lcd.print(dc[3]);
    } else{
        lcd.setCursor(0, 0);
	    lcd.print("AC (Vrms)");
	    lcd.setCursor(0, 2);
	    lcd.print("V1: ");
	    lcd.print(ac[0]);
	    lcd.setCursor(0, 3);
	    lcd.print("V2: ");
	    lcd.print(ac[1]);
	    lcd.setCursor(0, 4);
	    lcd.print("V3: ");
	    lcd.print(ac[2]);
	    lcd.setCursor(0, 5);
	    lcd.print("V4: ");
	    lcd.print(ac[3]);
    }
}

void sendSerial(){
    // char buffer[50];
    for (short i = 0; i < 4; i++){
        // sprintf(buffer, "V%d,%f,%f", i+1, dc[i], ac[i]);
        // Serial.println(buffer);
        Serial.print('V');
        Serial.print(i+1);
        Serial.print(',');
        Serial.print(dc[i]);
        Serial.print(',');
        Serial.print(ac[i]);
        Serial.print('\n');
    }
}

void checkVolt(){
    for (short i = 0; i < 4; i++){
        if ( (abs(dc[i]) > 11.9 ) || (abs(ac[i]) > 11.9 ) ){
            digitalWrite(LEDs[i], HIGH);
        } else{
            digitalWrite(LEDs[i], LOW);
        }
    }
}