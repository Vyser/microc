from serial import Serial
import csv

ser = Serial('/tmp/ttyS1', '9600',timeout=1) #Se abre la comunicación serial. Se debe cambiar el puerto.
with open('medidas.csv', 'w') as csvfile: #Se abre el archivo csv para escritura.

	writer = csv.writer(csvfile)
	writer.writerow(["Canal","ComponenteDC","ComponenteAC"])
	while 1:
		line = ser.readline() #Se lee del puerto serial, y se verifica si no es vacío
		if line:
			line = line[0:len(line)-1].decode("utf-8")
			print(line)
			info = line.split(",")
			writer.writerow(info) #Se escribe al archivo
