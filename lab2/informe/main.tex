\input{preamble}

\pagenumbering{roman}

\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\title{
{
    \begin{tikzpicture}[overlay, remember picture]
        \node[anchor=north west, %anchor is upper left corner of the graphic
            xshift=3cm, %shifting around
            yshift=-3.45cm] 
            at (current page.north west) %left upper corner of the page
        {\includegraphics[height=1.9cm]{img/EIE_v.png}}; 
    \end{tikzpicture}
    \begin{tikzpicture}[overlay, remember picture]
        \node[anchor=north east, %anchor is upper left corner of the graphic
            xshift=-2.5cm, %shifting around
            yshift=-3.65cm] 
            at (current page.north east) %left upper corner of the page
        {\includegraphics[height=1.4cm]{img/ucr.png}}; 
    \end{tikzpicture}
    \Large 
        \textbf{Universidad de Costa Rica}\\
        Facultad de Ingeniería\\
        Escuela de Ingeniería Eléctrica\\~\\
        \vspace{3cm} \texttt{IE-0624} Laboratorio de Microcontroladores
    }
    ~\\~\\\vspace{3cm} 
    {\textbf{\LARGE Laboratorio 2}}\\
    \vspace{3cm}
}

\pagestyle{fancy}
\fancyhf{}
\lhead{IE-0624: Laboratorio de Microcontroladores}
\chead{}
\rhead{Laboratorio 2}
\lfoot{Escuela de Ingeniería Eléctrica}
% \cfoot{\thepage\ de \pageref{LastPage}}
\cfoot{\thepage}
\rfoot{Universidad de Costa Rica}

\author{Victor Manuel Yeom Song - \texttt{B78494}
    }

\date{II-2020}

\makeatletter
\g@addto@macro{\UrlBreaks}{\UrlOrds}
\makeatother
\begin{document}

\maketitle

\newpage
\tableofcontents

\newpage
\pagenumbering{arabic}

\section{Resumen}

En este trabajo se presenta un semáforo inteligente utilizando LEDs, botones y el microcontrolador ATtiny4313. Se modela un sistema real de semáforos mediante el uso de los LEDs y los botones representan el botón que uno presionaría en un paso peatonal. 

El funcionamiento general del sistema de semáforos se modela como una máquina de estados. Al inicio de un ciclo de funcionamiento, los semáforos vehiculares se mantienen en verde por al menos 10 segundos aunque uno presione un botón. Si uno presiona el botón durante este lapso, apenas pasen los 10 segundos, se produce un cambio en las luces; las luces verdes vehiculares empiezan a parpadear para indicar el eventual cambio a rojo, luego se cambian a rojo y los semáforos peatonales se mantienen verdes por 10 segundos. Luego de 10 segundos, estos empiezan a parpadear para simbolizar la transición a rojo, y luego se inicia otro ciclo de funcionamiento. Después de que pasan 10 segundos sin presionar el botón, en cualquier momento que se presione el botón se inicia la transición descrita.

Se logró realizar la implementación de forma exitosa, con la cual se vio la utilidad del manejo de interrupciones y la temporización en circuitos digitales. Esta herramienta le da bastante poder al empleo de microcontroladores en varios posibles contextos y muestra una versatilidad amplia.

\newpage
\section{Nota teórica}

\subsection{El microcontrolador ATtiny4313}

\subsubsection{Características eléctricas}

El microcontrolador ATtiny4313 tiene las siguientes características eléctricas, descritas en \cite{AVR}:

\begin{itemize}
    \item Tensión de operación entre \SI{1,8}{V} y \SI{5,5}{V}.
    \item Consumo de corriente de \SI{190}{\micro A} a \SI{1,8}{V} y \SI{1}{MHz} en modo activo, \SI{24}{\micro A} a \SI{1,8}{V} y \SI{1}{MHz} en modo inactivo y \SI{0,1}{\micro A} a \SI{1,8}{V} en modo de baja potencia, con una temperatura de \SI{25}{\degree C}.
    \item Velocidad de operación máxima de \SI{4}{MHz} a \SI{1,8}{V}, \SI{10}{MHz} a \SI{2,7}{V} y \SI{20}{MHz} a \SI{4,5}{V}.
    \item Arquitectura RISC con 120 instrucciones, de las cuales la mayoría se ejecuta en un ciclo de reloj.
    \item Memoria flash de \SI{4}{KB}, EEPROM de \SI{256}{B} y RAM de \SI{256}{B}.
    \item 32 registros de propósito general.
    \item Un timer de 8 bits y uno de 16 bits, ambos con prescaler y modo de comparación. 
    \item 18 líneas de entrada/salida programables y 20 pines PDIP, 20 SOIC y 20 MLD/VQFN.
\end{itemize}

Su diagrama de pinout es dado por la figura \ref{fig:pinout}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .5\linewidth]{img/background/ATtinypinout.png}
    \caption{Diagrama del ATtiny4313. Recuperado de \cite{AVR}.}
    \label{fig:pinout}
\end{figure}


\subsubsection{Periféricos}
En cuanto a capacidades periféricas del pinout, los pines del microcontrolador son descritos en \cite{AVR}:

\begin{itemize}
    \item Pines de alimentación VCC y GND.
    \item Puerto A, compuesto por los pines PA0 a PA2, puerto B, compuesto por los pines PB0 a PB7 y puerto C, compuesto por los pines PD0 a PD6. Todos estos pines tienen funcionalidad de entrada/salida bidireccional y resistencias internas de pull-up. Los buffers de salida de estos pines tienen características de control simétricas y alta capacidad de fuente y drenaje. Con este puerto se manejan las interrupciones externas por botón.
    \item RESET: Entrada de reset en el pin PA2. Si está en bajo durante más tiempo que la duración mínima de pulso se genera una señal de reset aunque el reloj no esté corriendo.
    \item XTAL1: Localizado en el pin PA0, funciona como entrada del amplificador oscilador inversor y entrada para el reloj interno.
    \item XTAL2: Localizado en el pin PA1, funciona como salida del amplificador oscilador inversor.
\end{itemize}

En cuanto a puertos, para este laboratorio se utiliza únicamente el puerto B. Los registros asociados a este puerto se muestran en la figura \ref{fig:portB}, pero cabe resaltar que los pines de cada puerto tienen la misma configuración de registros y bits, con cada bit siendo definido por \texttt{DDxn}, \texttt{PORTxn} y \texttt{PINxn}, donde \texttt{x} corresponde al puerto y \texttt{n} al número de bit del registro.

\begin{figure}[H]
    \centering
    \includegraphics[width = .7\linewidth]{img/background/portB.png}
    \caption{Registros del puerto B. Recuperado de \cite{AVR}.}
    \label{fig:portB}
\end{figure}

El bit \texttt{DDxn}, en el registro \texttt{DDRx}, define la dirección del pin correspondiente; 1 define que es una salida y 0 que es una entrada. 

El bit \texttt{PORTxn} define el estado del pin del puerto correspondiente. Si se configura como entrada y tiene un 1, la resistencia pull-up se activa y para apagarla se debe poner un 0 en el bit, o bien configurar el pin como salida. Por otro lado, si el pin está configurado como salida y \texttt{PORTxn} está en 1, el estado del pin está en alto, mientras que si estuviera en 0 el pin se mantiene en bajo.

El bit \texttt{PINxn} invierte el valor de \texttt{PORTxn} si \texttt{PINxn} = 1, independientemente del valor de \texttt{DDxn}. A partir del registro \texttt{PINx} se puede leer \texttt{DDxn}.

\subsubsection{Temporización}

En este laboratorio se utilizó el timer 0. Este emplea los siguientes registros:

\begin{itemize}
    \item TCNT0: Registro de 16 bits que contiene el valor del timer y se actualiza según la frecuencia que se define en el registro TCCR1B.
    \item OCR0A/B: Registros de 16 bits que contienen el valor al que se desea llegar con el timer. Si llega a dicho valor, levanta una bandera en el registro TIFR.
    \item TCCR0B: Compuesto por los bits mostrados en la figura \ref{fig:TCCR}.
    
    \begin{figure}[H]
        \centering
        \includegraphics[width = .7\linewidth]{img/background/TCCR.png}
        \caption{Registro TCCR0B. Recuperado de \cite{AVR}.}
        \label{fig:TCCR}
    \end{figure}
    
    Sus funciones principales son de control para el timer. Por ejemplo, definen el modo de operación y el prescaler a utilizar. Los bits de interés principal son:
    
    \begin{itemize}
        \item WGM (Waveform Generation Mode): definen el modo de operación. Interesan principalmente el modo normal, obtenido con WGM=0, y el modo CTC (Clear Timer on Compare match), modo con el que se reinicia el timer cuando OCR0A/B sea igual a TCNT1, obtenido con WGM12=1, WGM11=0 y WGM10=0.
        \item CS (Clock Select): bits con los que se configuran la operación del reloj. Estos bits y sus funciones asociadas se detallan en la figura \ref{fig:CSbits}.
        
        \begin{figure}[H]
            \centering
            \includegraphics[width = .7\linewidth]{img/background/CS.png}
            \caption{Descripción de las funciones con los bits de CS. Recuperado de \cite{AVR}.}
            \label{fig:CSbits}
        \end{figure}
        
        Con preescalamiento, se define la del timer por medio de \eqref{eqn:workFreq}, según \cite{AVR}:
        
        \begin{equation}
            \label{eqn:workFreq}
            f = \frac{f_{clk}}{2N(1+\text{\texttt{OCRnx}})}
        \end{equation}
        
        Con $f_{clk}$ la frecuencia interna del microcontrolador, tomada en \SI{1}{MHz}, N el factor de preescalamiento y \texttt{OCRnx} el valor que se compara con el timer. Para el timer utilizado en el laboratorio se compara con los registros OCR0A.
        
    \end{itemize}
\end{itemize}

\newpage
\subsubsection{Interrupciones}

Para habilitar el uso de interrupciones en el microcontrolador, se hace uso del registro SREG, cuyos bits se presentan en la figura \ref{fig:SREG}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .7\linewidth]{img/background/SREG.png}
    \caption{Bits del registro SREG. Recuperado de \cite{AVR}.}
    \label{fig:SREG}
\end{figure}

El bit de interés en este caso es el bit I, referido por el bit 7. Este bit debe estar en alto para poder habilitar las interrupciones globales. Las interrupciones que se pueden manejar en la arquitectura se definen por los vectores mostrados en la figura \ref{fig:interruptVect}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .6\linewidth]{img/background/interruptVect.png}
    \caption{Vectores de interrupción y Reset del microcontrolador. Recuperado de \cite{AVR}.}
    \label{fig:interruptVect}
\end{figure}

Para el laboratorio, son de principal interés los vectores dados por los labels PCINT1 (utilizado para las interrupciones de botón) y TIMER0 COMPA (para interrupciones de timer). Para ver por qué, se deben ver los registros PCMSK1 y TIMSK primero. 

El registro PCMSK1 está compuesto por los bits mostrados en la figura \ref{fig:PCMSK1}.

\begin{figure}[H]
    \centering
    \includegraphics[width=.7\linewidth]{img/background/PCMSK1.png}
    \caption{Bits del registro PCMSK1. Recuperado de \cite{AVR}.}
    \label{fig:PCMSK1}
\end{figure}

Estos bits habilitan las interrupciones por medio de los pines 8 a 10, los cuales según el pinout corresponden a los pines PA0 a PA2. Cuando se detecta una interrupción por medio del pin PA0, se levanta el vector de interrupción PCINT1, con lo que se puede manejar el estado del botón por medio del pin PA0 y las interrupciones asociadas.

El registro TIMSK está compuesto por los bits mostrados en la figura \ref{fig:TIMSK}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .7\linewidth]{img/background/TIMSK.png}
    \caption{Bits del registro TIMSK. Recuperado de \cite{AVR}.}
    \label{fig:TIMSK}
\end{figure}

En el caso de este laboratorio, es de principal interés el bit OCIE0A. Cuando el timer llega al valor escrito OCR0A, lo cual se verifica por la configuración del registro TCCR0B y la bandera TIFR, se levanta un 1 en este bit y con esto se levanta la interrupción TIMER0 COMPA. De esta manera, se puede manejar una variable temporal en el programa que maneje las duraciones que se deseen trabajar.

\subsection{El diodo semiconductor}

Los diodos semiconductores son dispositivos que permiten el flujo de la corriente en una dirección pero no en el otro. En \cite{Boylestad} se indica que son creados a partir de la unión de un material tipo p (silicio dopado con algún elemento como el boro) con otro tipo n (silicio dopado con algún elemento como el fósforo). También se indica que si uno deseara un LED, simplemente se agregan materiales al silicio o el encapsulado que reaccionan a los campos eléctricos y generan luz.

La ecuación que describe el comportamiento del diodo es dada por \eqref{eqn:diode}:

\begin{equation} \label{eqn:diode}
    I_D=I_s \left (e^{\frac{V_D}{V_T}}-1 \right)
\end{equation}

Donde $I_D$ es la corriente a través del diodo, $I_S$ es la corriente de saturación inversa, $V_D$ la tensión a través del diodo y $V_T$ el voltaje térmico, determinado por \eqref{eqn:threshold}:

\begin{equation}\label{eqn:threshold}
    V_T=\frac{\kappa T}{q}
\end{equation}

Donde $\kappa$ es la constante de Boltzmann, T la temperatura en kelvins y q la carga del electrón.

\subsection{Rebote de switches}

En \cite{Bounce} se indica que en la vida real, cuando se presiona un botón o switch, se produce el contacto de dos partes de metal. Este contacto puede parecer instantáneo, pero durante el momento de activación del botón se pueden dar varios contactos pequeños antes del cierre total del botón o switch, por lo que las señales eléctricas que produce el switch ``rebotan'' entre apagado y encendido en los momentos previos al cierre total del mismo. Se pueden producir entre 10 y 100 de estos rebotes en \SI{1}{ms}, lo cual resulta problemático dado que el hardware utilizado en la actualidad puede operar a velocidades más altas, por lo que puede interpretar esos rebotes como varias activaciones del switch.

Para lidiar con estos rebotes, generalmente se utilizan capacitores para que filtren estas oscilaciones. Para el laboratorio, además de un capacitor, se utiliza una resistencia para limitar la corriente que se puede producir a la entrada del microcontrolador, así como facilitar la carga y descarga segura del capacitor cuando se presiona el botón. La adición de estos elementos no produciría un efecto muy visible en la simulación en la herramienta SimulIDE, dado que los switches en la misma son ideales y no producen rebote, pero se toman en cuenta para una eventual implementación en la vida real.

\section{Lista de componentes}

La lista de componentes necesarios para desarrollar el diseño se presentan en el cuadro \ref{tab:components}.

\begin{table}[H]
    \centering
    \begin{tabular}{cccc}
        \toprule
        Componente & Valor nominal & Precio & Cantidad \\ \midrule
        Resistencia \SI{1/4}{W} & \SI{200}{\ohm} & \$5,4 / 10 piezas & 6 \\ \hline
        Resistencia \SI{1/4}{W} & \SI{100}{\ohm} & \$5,53 / 10 piezas & 2 \\ \hline
        \multirow{2}{*}{LED TLUR6400} & $V_{fw} = \SI{2}{V}$ & \multirow{2}{*}{\$3,30 / 10 piezas} & \multirow{2}{*}{3} \\
        &  $I_{fw, max} = \SI{20}{mA}$ & & \\\hline
        \multirow{2}{*}{LED TLHP42J2L1} & $V_{fw} = \SI{2,2}{V}$ & \multirow{2}{*}{\$4,32 / 10 piezas} & \multirow{2}{*}{3} \\
        &  $I_{fw, max} = \SI{30}{mA}$ & & \\\hline
        Capacitor cerámico & \SI{47}{nF} & \$0,25 / 1 pieza & 1 \\\hline
        ATtiny4313 & --- & \$0,77 / 1 unidad & 1 \\ \bottomrule
    \end{tabular}
    \caption{Lista de componentes para realizar el diseño.}
    \label{tab:components}
\end{table}

La información sobre los costos y aspectos de los componentes se obtuvieron en las siguientes direcciones URL:

\begin{itemize}
    \item Resistencia de \SI{200}{\ohm}: \newline \url{https://www.amazon.com/Projects-Resistors-Watt-Choose-Quantity/dp/B071HLFRF5}
    
    \item Resistencia de \SI{100}{\ohm}: \newline \url{https://www.amazon.com/Projects-Resistors-Watt-Choose-Quantity/dp/B0725G2WM7}
    
    \item LED rojo: \url{https://www.mouser.co.cr/ProductDetail/Vishay-Semiconductors/TLUR6400?qs=\%2Fha2pyFaduheQTaVa\%252BYMbL2buDTtPc2eNvmnn2P4l2M=}
    
    \item LED verde: \url{https://www.mouser.co.cr/ProductDetail/Vishay-Semiconductors/TLHP42J2L1?qs=5csRq1wdUj7NkJHISYxk\%252BA==}
    
    \item Capacitor cerámico: \url{https://www.jameco.com/z/DC-047-Capacitor-Ceramic-Disc-0-047-micro-F-47nF-50V-plusmn-20-_15253.html}
    
    \item ATtiny4313: \url{https://www.microchip.com/wwwproducts/en/ATTINY4313}
\end{itemize}

\newpage
\section{Diseño}

Se plantea un diseño con ``ramas'' de LEDs conectadas al microcontrolador, con cada rama representando semáforos vehiculares o peatonales. Para los peatonales, cada rama tendría 2 LEDs en paralelo, mientras que los vehiculares tendrán uno por rama, de modo que se modela la situación de una calle real con dos aceras. Así, se tendrían 4 ramas, cada una conectada a uno de los pines del puerto B del microcontrolador. 

Cada rama debe tener una resistencia asociada que limite la corriente que corre por los LEDs (con corriente de \textit{forward} máxima de \SI{20}{mA} como se indica en \cite{LED}), así como limitar la corriente que consumen dado que el ATtiny4313 tiene una corriente de suministro máxima de \SI{40}{mA} en cada pin GPIO \cite{AVR}). Como se utilizan cuatro pines de salida, siempre y cuando se mantenga una corriente menor a \SI{20}{mA} en cada rama, no deberían haber problemas en este aspecto. También se tiene que por default cada pin de salida produce una tensión de \SI{5}{V}.

Se tiene la rama de una resistencia y un LED, dada en la figura \ref{fig:designR_1LED}:

\begin{figure}[H]
    \centering
    \includegraphics[width = .4\linewidth]{img/design/designR_1LED.png}
    \caption{Circuito de diseño para la rama de 1 LED. Elaboración propia.}
    \label{fig:designR_1LED}
\end{figure}

Nótese que aquí se toma el modelo simplificado del diodo y no se toman en cuenta sus propiedades resistivas y capacitivas, lo cual puede inducir un error en los resultados finales. Estos aspectos físicos no se toman en cuenta dado que pueden variar mucho entre diodo y diodo, no se indican en \cite{LED} y la relación entre corriente y tensión no es lineal, como se modela por la ecuación del diodo de Shockley.

También se debe considerar que en una rama con 2 LEDs, como los LEDs están en paralelo, la tensión es la misma en ambos, por lo que el diseño aplica igual para estas ramas, siempre y cuando la corriente no exceda la máxima que puede suministrar un pin.

Se define una corriente $I_{1D} = \SI{15}{mA}$, menor a la corriente máxima de \SI{20}{mA} del LED, para diseñar la resistencia R. Por una LTK, se tiene:

\begin{eqnarray*}
    I_{1D}R + 2 &=& 5\\
    \iff R &=& \frac{5-2}{I_{1D}}\\
    \iff R &=& \SI{200}{\ohm}
\end{eqnarray*}

Por simpleza, se puede tomar este valor de resistencia y comprobar la corriente que tomaría un LED verde, con una tensión $V_{fw}$ de \SI{2,2}{V}. Se hace esto dado que la tensión de activación no cambia mucho entre los colores de LED. 

Entonces, para una rama con un LED verde se tendría:

\begin{eqnarray*}
    I_{LED}\cdot 200 + 2,2 &=& 5\\
    \iff I_{LED} &=& \frac{5-2,2}{200}\\
    \iff I_{LED} &=& \SI{14}{mA}
\end{eqnarray*}

La corriente para un LED verde se reduciría en solo \SI{1}{mA}, por lo que se mantiene este valor de resistencia para ramas con LEDs verdes. 


En ramas con 2 LEDs, por ley de corrientes de Kirchhoff, la corriente total que debe suministrar el microcontrolador es la necesaria para alimentar 2 LEDs. Con una corriente de \SI{15}{mA} por LED, la corriente máxima de suministro llega a \SI{30}{mA} por pin, que se mantiene por debajo del límite de \SI{40}{mA} establecido por el fabricante, por lo que se considera que el diseño en general no tendrá problemas.

Ahora, para evitar el efecto del rebote del botón que se tendría en la vida real, como se hace en \cite{Bounce}, simplemente se agrega un circuito RC asociado al botón. El capacitor se encarga de filtrar los pulsos rápidos del rebote, mientras que las resistencias sirven como la ruta de carga y descarga del capacitor, además de limitar la corriente que entra al microcontrolador. Se toman una capacitancia de \SI{47}{nF} y dos resistencias de \SI{100}{\ohm} cada una para tener una constante de tiempo pequeña, de manera que el ``delay'' entre la entrada y la salida sea pequeña. 

El diseño final del circuito queda como se muestra en la figura \ref{fig:circDesign}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .8\linewidth]{img/design/circDesign.png}
    \caption{Diseño del circuito en SimulIDE. Elaboración propia.}
    \label{fig:circDesign}
\end{figure}

Aquí, se puede ver que se utiliza el pin A0 como entrada para manejar las interrupciones por botones y los pines B1 a B4 como salidas.

\newpage
Para el diseño del programa, se sigue el diagrama de temporización proveído en el enunciado, dado en la figura \ref{fig:tempDiag}.

\begin{figure}[H]
    \centering
    \includegraphics[width = .8\linewidth]{img/design/tempDiag.png}
    \caption{Diagrama de temporización de los estados a diseñar, proveído en el enunciado.}
    \label{fig:tempDiag}
\end{figure}

Aquí, se tienen los estados:

\begin{itemize}
    \item LDPV: LEDs de paso vehicular.
    \item LDPP: LEDs de paso peatonal.
    \item LDVD: LEDs de vehículos detenidos.
    \item LDPD: LEDs de peatones detenidos.
\end{itemize}

El diseño del programa se resume con el diagrama de flujo mostrado en la figura \ref{fig:flow}. Para este diagrama, se tienen la siguiente definición de variables utilizadas:

\begin{itemize}
    \item \texttt{button}: Variable global tipo \texttt{short} que guarda el estado del botón. Almacena un 0 cuando no se ha presionado el botón en un ciclo de funcionamiento, mientras que si se levanta una excepción por el botón almacena un 1.
    \item \texttt{times}: Variable global tipo \texttt{int} utilizada para manejar el tiempo. En el programa se configura un valor de \texttt{OCR0A} de 245 y el microcontrolador opera a \SI{1}{MHz}, por lo que utilizando \eqref{eqn:workFreq} se puede ver que se levanta una excepción TIMER0 COMPA aproximadamente cada \SI{0,25}{s}. Se utiliza esta excepción para manejar el valor de \texttt{times}, de manera que \texttt{times} representa un múltiplo de \SI{0,25}{s} que ha pasado desde un punto de interés en el tiempo. Por ejemplo, si \texttt{times} tiene un valor de 4, ha pasado 1 segundo.
    \item \texttt{passTime}: Variable global tipo \texttt{short} utilizada para manejar un lapso de tiempo de \SI{10}{s}. Al inicio de un ciclo de funcionamiento, se deben esperar al menos \SI{10}{s} para que \texttt{button}=1 tenga un efecto sobre los semáforos. Esta variable maneja esta condición, almacenando un 0 si no se han pasado \SI{10}{s} y un 1 si se ha esperado esta cantidad de tiempo. Se registra el tiempo por medio de \texttt{times}.
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width = .8\linewidth]{img/design/flow.png}
    \caption{Diagrama de flujo que describe el funcionamiento del programa. Elaboración propia.}
    \label{fig:flow}
\end{figure}

En este diagrama, un estado como por ejemplo LDPV OFF \& LDVD ON 1s indica que las luces LDPV se hallan apagadas y las luces LDVD se hallan encendidas, esto durante una duración de 1 segundo. El manejo de estos tiempos se realiza por medio de la variable \texttt{times} descrita anteriormente.

\newpage
\section{Análisis de resultados}

En la figura \ref{fig:tempResult} se presentan los resultados de la temporización. La línea amarilla representa la señal del botón, la anaranjada corresponde a LDPV, la azul corresponde a LDPD y la verde a LDPP. Se puede ver que cada una de las líneas corresponde a las líneas presentadas en la figura \ref{fig:tempDiag}, con las mismas duraciones en cada etapa. Además, se ve que aunque se presiona el botón a los 3 segundos de iniciada la simulación, la transición de los estados no ocurre hasta el segundo 10. Por lo anterior, se considera que se realiza la temporización de forma exitosa.

\begin{figure}[H]
    \centering
    \includegraphics[width = \linewidth]{img/results/tempResult.png}
    \caption{Resultado de la temporización implementada. Elaboración propia.}
    \label{fig:tempResult}
\end{figure}

En la figura \ref{fig:curr2green} se presenta el funcionamiento cuando hay paso de peatones, midiendo la corriente. Se ve que para una rama con 2 LEDs verdes se suministran \SI{20}{mA}, en lugar de los \SI{28}{mA} que uno esperaría con el diseño realizado. Esto se puede deber a que no se toman en cuenta las características resistivas ni capacitivas de los LEDs durante el diseño, al realizar este con el modelo simplificado del diodo. También se puede considerar un error relacionado a los redondeos que realiza la plataforma de simulación. Otro aspecto que puede afectar este diseño es la resistencia de pull-up de cada pin, la cual no se tomó en cuenta para el diseño y puede afectar la corriente que se suministra a cada rama.

Considerando lo anterior, se tiene que el circuito funciona de manera esperada, dado que la corriente no excede los límites de ninguno de los componentes, por lo que no habría riesgo de dañarlos.

\begin{figure}[H]
    \centering
    \includegraphics[width = .7\linewidth]{img/results/current2green.png}
    \caption{Luz verde peatonal encendida. Elaboración propia.}
    \label{fig:curr2green}
\end{figure}

\newpage
En la figura \ref{fig:regularFunc} se presenta el circuito funcionando cuando no se presiona ningún botón. Simplemente se mantienen los semáforos peatonales rojos y el semáforo vehicular verde encendidos, mostrando la funcionalidad normal del diseño.

\begin{figure}[H]
    \centering
    \includegraphics[width = .7\linewidth]{img/results/regularFunc.png}
    \caption{Funcionamiento sin presionar el botón. Elaboración propia.}
    \label{fig:regularFunc}
\end{figure}

\section{Conclusiones y recomendaciones}

Se vio la utilidad de los timers y las interrupciones en el diseño de circuitos digitales en la realización del laboratorio, utilizando estos para manejo de señales cronometradas y generación de patrones de salida.

El uso de las banderas y la configuración del timer resulta fácil una vez que uno entiende cómo funcionan, de modo que la tarea de manejar señales temporales se simplifica bastante con el modelo de interrupciones.

Es importante considerar la frecuencia de operación del microcontrolador, dado que si no se toma en cuenta la temporización se puede ver afectada y no funcionar como se espera.

Si se desea realizar un circuito más preciso en cuando a medidas eléctricas, sería recomendable considerar las resistencias de pull-up así como las propiedades físicas de cada componente al realizar el diseño del circuito electrónico.

% \bibliography{annot}

\begin{thebibliography}{IEEEannot}

\bibitem{Bounce} Jens Christoffersen. ``Switch Bounce and How to Deal with It'', 2015. [En línea]. Disponible en: \url{https://www.allaboutcircuits.com/technical-articles/switch-bounce-how-to-deal-with-it/}

\bibitem{Boylestad} Robert L. Boylestad \& Louis Nashelsky, \textit{Electrónica: Teoría de Circuitos y Dispositivos Electrónicos}. Pearson Education, 2009.

\bibitem{LED} Vishay Semiconductors, ``Universal LED in $\varnothing$ \SI{5}{mm} Tinted Difussed Package'', 2013. [En línea]. Disponible en: \url{https://www.vishay.com/docs/83171/tlur640.pdf}

\bibitem{AVR} Atmel, ``8-bit AVR Microcontroller with 2/4K Bytes In-System Programmable Flash'', 2011.

\end{thebibliography}

\end{document}
