Laboratorio 2
==========
--------------------
Estudiante:
--------------------
+ Victor Manuel Yeom Song - B78494

En este repositorio se encuentra el trabajo realizado para el Laboratorio 2 del curso IE-0624 Laboratorio de Microcontroladores. 

En el directorio src se hallan el código fuente y los archivos de simulación necesarios para la simulación del diseño en SimulIDE. El archivo stoplight.simu contiene el circuito implementado en SimulIDE y el archivo stoplight.c el código implementado en C para la funcionalidad del ATtiny4313. 

Para poder realizar la simulación, se debe compilar el archivo stoplight.c (el Makefile de src se encarga de esto) de donde se obtiene un archivo de extensión .hex, llamado "main.hex". Luego, con el archivo stoplight.simu abierto en SimulIDE, se debe cargar el *firmware* al microcontrolador. Este firmware corresponde al archivo main.hex.

En el directorio informe se hallan los archivos necesarios para generar el informe del laboratorio. Se incluye un Makefile que compila los archivos y abre el documento pdf generado.