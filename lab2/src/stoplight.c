#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

void pedestrianWalk();
// Variable global para mostrar el estado del boton:
short button = 0; // se inicializa apagado
// Variable global para el manejo de contadores de tiempo:
int times = 0;	//representa la cantidad de cuartos de segundo (0,25088 s) que han transcurrido
short passTime = 0;

// Para los interrupts:

ISR(PCINT1_vect){ // Rutina de interrupción para cuando se presiona un botón
	if (!button){
		button = 1;
	}
}

// Esta rutina genera una interrupción cada 0,25088 segundos (@1MHz), se manejara el tiempo con la variable times
ISR(TIMER0_COMPA_vect){
    times++;
    if (times == 40){ // Se maneja el paso de 10 s necesario para el funcionamiento
        passTime = 1;
    }
}

int main(void)
{
    SREG = 0x80;   // Se setea el global interrupt enable bit, 0x80 habilita el uso de interrupciones
    DDRB = 0x1E; // Configuracion de puertos
    DDRA = 0x00;
    GIMSK = 0x08;  // Configuración del Interrupt Mask Register para permitir interrupts en PCI1
    PCMSK1 = 0x01; // Interrupts por cambios en el pin A0

    TCCR0B = 0x05; // Prescaler de 1024
	TIMSK = 0x01;  // Para habilitar interrupts por comparacion del contador
	OCR0A = 0xF5;  // 245 (decimal), ya que 245*1024/1MHz = 0.25088 s (para contar cuartos de segundo)
	TCCR0A = 0xC2; //Para que se setee OC0A al cumplirse la comparacion del timer con OCR0A

    while(1)
    {
        // Se revisa si han pasado 10 s
        if (!passTime){
            PORTB = 0x0C;
        }
        // Cuando pasan 10 s, se espera por el estado del botón
        else if (passTime) {
            if (!button){
                PORTB = 0x0C; // LDPV y LDPP encendidos
            }
            else{
                pedestrianWalk();
            }
        }
    }
}

void pedestrianWalk(){
    times = 0;
    // Parpadean las luces verdes vehiculares en preparación al paso de los peatones
    for (short i = 0; i < 3; i++){
        while (times < 2){
            PORTB &= ~(1 << PORTB2);
            continue;
        }
        times = 0;
        while (times < 2){
            PORTB |= (1 << PORTB2);
        }
        times = 0;
    }

    // Se encienden las luces verdes peatonales y se apagan las rojas peatonales
    while (times < 4){
        PORTB |=  (1 << PORTB1);
        PORTB &=  ~(1 << PORTB2);
        continue;
    }
    times = 0;

    // Paso de los peatones
    while (times < 40){
        PORTB &=  ~(1 << PORTB3);
        PORTB |=  (1 << PORTB4);
        continue;
    }
    times = 0;

    // Parpadean las luces peatonales verdes en preparación para el paso de vehiculos
    for (short i = 0; i < 3; i++){
        while (times < 2){
            PORTB &=  ~(1 << PORTB4);
        }
        times = 0;
        while (times < 2){
            PORTB |=  (1 << PORTB4);
        }
        times = 0;
    }

    while (times < 4){
        PORTB |=  (1 << PORTB3);
        PORTB &=  ~(1 << PORTB4);
        continue;
    }

    PORTB = 0x0C;
    passTime = 0; // Se deben contar 10 segundos otra vez antes de que se pueda volver a recibir el boton
    button = 0; // Se resetea el estado del boton
}

